﻿namespace Rent_A_Car
{
    partial class AddCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddCustomer));
            this.grpbxpersonal = new System.Windows.Forms.GroupBox();
            this.comboGender = new System.Windows.Forms.ComboBox();
            this.dateTimeBirth = new System.Windows.Forms.DateTimePicker();
            this.txtIssue = new System.Windows.Forms.TextBox();
            this.txtLicence = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtCell = new System.Windows.Forms.TextBox();
            this.txtPlace = new System.Windows.Forms.TextBox();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.lblIssue = new System.Windows.Forms.Label();
            this.lblLicence = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblCell = new System.Windows.Forms.Label();
            this.lblPlace = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.grpbxcar = new System.Windows.Forms.GroupBox();
            this.dailyPrice = new System.Windows.Forms.Button();
            this.lblDailyPrice = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.comboModel = new System.Windows.Forms.ComboBox();
            this.comboTransmission = new System.Windows.Forms.ComboBox();
            this.comboFuel = new System.Windows.Forms.ComboBox();
            this.comboColor = new System.Windows.Forms.ComboBox();
            this.comboBrand = new System.Windows.Forms.ComboBox();
            this.lblColor = new System.Windows.Forms.Label();
            this.lblFuel = new System.Windows.Forms.Label();
            this.lblTransmission = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblBrand = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.grpbxrenting = new System.Windows.Forms.GroupBox();
            this.btnTotalPrice = new System.Windows.Forms.Button();
            this.lblDays = new System.Windows.Forms.Label();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.lblReceiving = new System.Windows.Forms.Label();
            this.dateTimeDue = new System.Windows.Forms.DateTimePicker();
            this.dateTimeReceiving = new System.Windows.Forms.DateTimePicker();
            this.lblDue = new System.Windows.Forms.Label();
            this.grpbxpersonal.SuspendLayout();
            this.grpbxcar.SuspendLayout();
            this.grpbxrenting.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpbxpersonal
            // 
            this.grpbxpersonal.Controls.Add(this.comboGender);
            this.grpbxpersonal.Controls.Add(this.dateTimeBirth);
            this.grpbxpersonal.Controls.Add(this.txtIssue);
            this.grpbxpersonal.Controls.Add(this.txtLicence);
            this.grpbxpersonal.Controls.Add(this.txtEmail);
            this.grpbxpersonal.Controls.Add(this.txtAddress);
            this.grpbxpersonal.Controls.Add(this.txtCell);
            this.grpbxpersonal.Controls.Add(this.txtPlace);
            this.grpbxpersonal.Controls.Add(this.txtSurname);
            this.grpbxpersonal.Controls.Add(this.txtName);
            this.grpbxpersonal.Controls.Add(this.txtId);
            this.grpbxpersonal.Controls.Add(this.lblIssue);
            this.grpbxpersonal.Controls.Add(this.lblLicence);
            this.grpbxpersonal.Controls.Add(this.lblEmail);
            this.grpbxpersonal.Controls.Add(this.lblAddress);
            this.grpbxpersonal.Controls.Add(this.lblCell);
            this.grpbxpersonal.Controls.Add(this.lblPlace);
            this.grpbxpersonal.Controls.Add(this.lblDate);
            this.grpbxpersonal.Controls.Add(this.lblGender);
            this.grpbxpersonal.Controls.Add(this.lblSurname);
            this.grpbxpersonal.Controls.Add(this.lblName);
            this.grpbxpersonal.Controls.Add(this.lblId);
            this.grpbxpersonal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.grpbxpersonal.Location = new System.Drawing.Point(12, 12);
            this.grpbxpersonal.Name = "grpbxpersonal";
            this.grpbxpersonal.Size = new System.Drawing.Size(298, 408);
            this.grpbxpersonal.TabIndex = 1;
            this.grpbxpersonal.TabStop = false;
            this.grpbxpersonal.Text = "Personal Information";
            // 
            // comboGender
            // 
            this.comboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboGender.FormattingEnabled = true;
            this.comboGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.comboGender.Location = new System.Drawing.Point(142, 124);
            this.comboGender.Margin = new System.Windows.Forms.Padding(2);
            this.comboGender.Name = "comboGender";
            this.comboGender.Size = new System.Drawing.Size(151, 28);
            this.comboGender.TabIndex = 4;
            // 
            // dateTimeBirth
            // 
            this.dateTimeBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dateTimeBirth.Location = new System.Drawing.Point(142, 189);
            this.dateTimeBirth.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimeBirth.Name = "dateTimeBirth";
            this.dateTimeBirth.Size = new System.Drawing.Size(151, 26);
            this.dateTimeBirth.TabIndex = 6;
            // 
            // txtIssue
            // 
            this.txtIssue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtIssue.Location = new System.Drawing.Point(142, 380);
            this.txtIssue.Margin = new System.Windows.Forms.Padding(2);
            this.txtIssue.MaxLength = 10;
            this.txtIssue.Name = "txtIssue";
            this.txtIssue.Size = new System.Drawing.Size(151, 26);
            this.txtIssue.TabIndex = 11;
            this.txtIssue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIssue_KeyPress);
            // 
            // txtLicence
            // 
            this.txtLicence.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtLicence.Location = new System.Drawing.Point(142, 348);
            this.txtLicence.Margin = new System.Windows.Forms.Padding(2);
            this.txtLicence.MaxLength = 6;
            this.txtLicence.Name = "txtLicence";
            this.txtLicence.Size = new System.Drawing.Size(151, 26);
            this.txtLicence.TabIndex = 10;
            this.txtLicence.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLicence_KeyPress);
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtEmail.Location = new System.Drawing.Point(142, 254);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(151, 26);
            this.txtEmail.TabIndex = 8;
            // 
            // txtAddress
            // 
            this.txtAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtAddress.Location = new System.Drawing.Point(142, 286);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(2);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(151, 56);
            this.txtAddress.TabIndex = 9;
            // 
            // txtCell
            // 
            this.txtCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtCell.Location = new System.Drawing.Point(142, 222);
            this.txtCell.Margin = new System.Windows.Forms.Padding(2);
            this.txtCell.MaxLength = 11;
            this.txtCell.Name = "txtCell";
            this.txtCell.Size = new System.Drawing.Size(151, 26);
            this.txtCell.TabIndex = 7;
            this.txtCell.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCell_KeyPress);
            // 
            // txtPlace
            // 
            this.txtPlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtPlace.Location = new System.Drawing.Point(142, 158);
            this.txtPlace.Margin = new System.Windows.Forms.Padding(2);
            this.txtPlace.Name = "txtPlace";
            this.txtPlace.Size = new System.Drawing.Size(151, 26);
            this.txtPlace.TabIndex = 5;
            this.txtPlace.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPlace_KeyPress);
            // 
            // txtSurname
            // 
            this.txtSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtSurname.Location = new System.Drawing.Point(142, 92);
            this.txtSurname.Margin = new System.Windows.Forms.Padding(2);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(151, 26);
            this.txtSurname.TabIndex = 3;
            this.txtSurname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSurname_KeyPress);
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtName.Location = new System.Drawing.Point(142, 60);
            this.txtName.Margin = new System.Windows.Forms.Padding(2);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(151, 26);
            this.txtName.TabIndex = 2;
            this.txtName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtName_KeyPress);
            // 
            // txtId
            // 
            this.txtId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtId.Location = new System.Drawing.Point(142, 30);
            this.txtId.Margin = new System.Windows.Forms.Padding(2);
            this.txtId.MaxLength = 11;
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(151, 26);
            this.txtId.TabIndex = 1;
            this.txtId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtId_KeyPress);
            // 
            // lblIssue
            // 
            this.lblIssue.AutoSize = true;
            this.lblIssue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblIssue.Location = new System.Drawing.Point(6, 383);
            this.lblIssue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIssue.Name = "lblIssue";
            this.lblIssue.Size = new System.Drawing.Size(56, 20);
            this.lblIssue.TabIndex = 30;
            this.lblIssue.Text = "Issue :";
            // 
            // lblLicence
            // 
            this.lblLicence.AutoSize = true;
            this.lblLicence.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblLicence.Location = new System.Drawing.Point(5, 351);
            this.lblLicence.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLicence.Name = "lblLicence";
            this.lblLicence.Size = new System.Drawing.Size(126, 20);
            this.lblLicence.TabIndex = 29;
            this.lblLicence.Text = "Drive Licence Id:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblEmail.Location = new System.Drawing.Point(5, 257);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(57, 20);
            this.lblEmail.TabIndex = 28;
            this.lblEmail.Text = "E-mail:";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAddress.Location = new System.Drawing.Point(5, 289);
            this.lblAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(72, 20);
            this.lblAddress.TabIndex = 27;
            this.lblAddress.Text = "Address:";
            // 
            // lblCell
            // 
            this.lblCell.AutoSize = true;
            this.lblCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblCell.Location = new System.Drawing.Point(5, 225);
            this.lblCell.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCell.Name = "lblCell";
            this.lblCell.Size = new System.Drawing.Size(88, 20);
            this.lblCell.TabIndex = 26;
            this.lblCell.Text = "Cell phone:";
            // 
            // lblPlace
            // 
            this.lblPlace.AutoSize = true;
            this.lblPlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPlace.Location = new System.Drawing.Point(5, 161);
            this.lblPlace.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPlace.Name = "lblPlace";
            this.lblPlace.Size = new System.Drawing.Size(110, 20);
            this.lblPlace.TabIndex = 25;
            this.lblPlace.Text = "Place Of Birth:";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblDate.Location = new System.Drawing.Point(5, 193);
            this.lblDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(106, 20);
            this.lblDate.TabIndex = 24;
            this.lblDate.Text = "Date Of Birth:";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblGender.Location = new System.Drawing.Point(5, 127);
            this.lblGender.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(67, 20);
            this.lblGender.TabIndex = 23;
            this.lblGender.Text = "Gender:";
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblSurname.Location = new System.Drawing.Point(5, 95);
            this.lblSurname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(78, 20);
            this.lblSurname.TabIndex = 22;
            this.lblSurname.Text = "Surname:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblName.Location = new System.Drawing.Point(5, 63);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(55, 20);
            this.lblName.TabIndex = 21;
            this.lblName.Text = "Name:";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblId.Location = new System.Drawing.Point(5, 33);
            this.lblId.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(87, 20);
            this.lblId.TabIndex = 20;
            this.lblId.Text = "Id Number:";
            // 
            // grpbxcar
            // 
            this.grpbxcar.Controls.Add(this.dailyPrice);
            this.grpbxcar.Controls.Add(this.lblDailyPrice);
            this.grpbxcar.Controls.Add(this.lblPrice);
            this.grpbxcar.Controls.Add(this.comboModel);
            this.grpbxcar.Controls.Add(this.comboTransmission);
            this.grpbxcar.Controls.Add(this.comboFuel);
            this.grpbxcar.Controls.Add(this.comboColor);
            this.grpbxcar.Controls.Add(this.comboBrand);
            this.grpbxcar.Controls.Add(this.lblColor);
            this.grpbxcar.Controls.Add(this.lblFuel);
            this.grpbxcar.Controls.Add(this.lblTransmission);
            this.grpbxcar.Controls.Add(this.lblModel);
            this.grpbxcar.Controls.Add(this.lblBrand);
            this.grpbxcar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.grpbxcar.Location = new System.Drawing.Point(316, 12);
            this.grpbxcar.Name = "grpbxcar";
            this.grpbxcar.Size = new System.Drawing.Size(278, 309);
            this.grpbxcar.TabIndex = 2;
            this.grpbxcar.TabStop = false;
            this.grpbxcar.Text = "Car Information";
            // 
            // dailyPrice
            // 
            this.dailyPrice.BackColor = System.Drawing.Color.Transparent;
            this.dailyPrice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dailyPrice.FlatAppearance.BorderSize = 0;
            this.dailyPrice.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.dailyPrice.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.dailyPrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dailyPrice.ForeColor = System.Drawing.Color.Transparent;
            this.dailyPrice.Image = ((System.Drawing.Image)(resources.GetObject("dailyPrice.Image")));
            this.dailyPrice.Location = new System.Drawing.Point(191, 233);
            this.dailyPrice.Margin = new System.Windows.Forms.Padding(2);
            this.dailyPrice.Name = "dailyPrice";
            this.dailyPrice.Size = new System.Drawing.Size(60, 67);
            this.dailyPrice.TabIndex = 17;
            this.dailyPrice.UseVisualStyleBackColor = false;
            this.dailyPrice.Click += new System.EventHandler(this.dailyPrice_Click);
            // 
            // lblDailyPrice
            // 
            this.lblDailyPrice.AutoSize = true;
            this.lblDailyPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblDailyPrice.Location = new System.Drawing.Point(117, 205);
            this.lblDailyPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDailyPrice.Name = "lblDailyPrice";
            this.lblDailyPrice.Size = new System.Drawing.Size(0, 25);
            this.lblDailyPrice.TabIndex = 53;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPrice.Location = new System.Drawing.Point(5, 210);
            this.lblPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(48, 20);
            this.lblPrice.TabIndex = 52;
            this.lblPrice.Text = "Price:";
            // 
            // comboModel
            // 
            this.comboModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboModel.FormattingEnabled = true;
            this.comboModel.Location = new System.Drawing.Point(121, 63);
            this.comboModel.Name = "comboModel";
            this.comboModel.Size = new System.Drawing.Size(142, 28);
            this.comboModel.TabIndex = 13;
            // 
            // comboTransmission
            // 
            this.comboTransmission.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTransmission.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboTransmission.FormattingEnabled = true;
            this.comboTransmission.Items.AddRange(new object[] {
            "Automatic",
            "Manuel"});
            this.comboTransmission.Location = new System.Drawing.Point(121, 99);
            this.comboTransmission.Margin = new System.Windows.Forms.Padding(2);
            this.comboTransmission.Name = "comboTransmission";
            this.comboTransmission.Size = new System.Drawing.Size(142, 28);
            this.comboTransmission.TabIndex = 14;
            // 
            // comboFuel
            // 
            this.comboFuel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboFuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboFuel.FormattingEnabled = true;
            this.comboFuel.Items.AddRange(new object[] {
            "Diesel",
            "Gasoline"});
            this.comboFuel.Location = new System.Drawing.Point(121, 135);
            this.comboFuel.Margin = new System.Windows.Forms.Padding(2);
            this.comboFuel.Name = "comboFuel";
            this.comboFuel.Size = new System.Drawing.Size(142, 28);
            this.comboFuel.TabIndex = 15;
            // 
            // comboColor
            // 
            this.comboColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboColor.FormattingEnabled = true;
            this.comboColor.Items.AddRange(new object[] {
            "White",
            "Black",
            "Metalic Grey",
            "Blue",
            "Red",
            "Yellow",
            "Orange",
            "Green"});
            this.comboColor.Location = new System.Drawing.Point(121, 171);
            this.comboColor.Margin = new System.Windows.Forms.Padding(2);
            this.comboColor.Name = "comboColor";
            this.comboColor.Size = new System.Drawing.Size(142, 28);
            this.comboColor.TabIndex = 16;
            // 
            // comboBrand
            // 
            this.comboBrand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboBrand.FormattingEnabled = true;
            this.comboBrand.Items.AddRange(new object[] {
            "BMW",
            "AUDI",
            "MERCEDES",
            "RENAULT",
            "VOLKSWAGEN",
            "SKODA",
            "SEAT",
            "PORSCHE",
            "VOLVO",
            "NISSAN",
            "TOYOTA"});
            this.comboBrand.Location = new System.Drawing.Point(121, 27);
            this.comboBrand.Margin = new System.Windows.Forms.Padding(2);
            this.comboBrand.Name = "comboBrand";
            this.comboBrand.Size = new System.Drawing.Size(142, 28);
            this.comboBrand.TabIndex = 12;
            this.comboBrand.SelectedIndexChanged += new System.EventHandler(this.comboBrand_SelectedIndexChanged);
            // 
            // lblColor
            // 
            this.lblColor.AutoSize = true;
            this.lblColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblColor.Location = new System.Drawing.Point(5, 174);
            this.lblColor.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(50, 20);
            this.lblColor.TabIndex = 43;
            this.lblColor.Text = "Color:";
            // 
            // lblFuel
            // 
            this.lblFuel.AutoSize = true;
            this.lblFuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFuel.Location = new System.Drawing.Point(5, 138);
            this.lblFuel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFuel.Name = "lblFuel";
            this.lblFuel.Size = new System.Drawing.Size(44, 20);
            this.lblFuel.TabIndex = 42;
            this.lblFuel.Text = "Fuel:";
            // 
            // lblTransmission
            // 
            this.lblTransmission.AutoSize = true;
            this.lblTransmission.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTransmission.Location = new System.Drawing.Point(5, 102);
            this.lblTransmission.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTransmission.Name = "lblTransmission";
            this.lblTransmission.Size = new System.Drawing.Size(106, 20);
            this.lblTransmission.TabIndex = 41;
            this.lblTransmission.Text = "Transmission:";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblModel.Location = new System.Drawing.Point(5, 66);
            this.lblModel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(56, 20);
            this.lblModel.TabIndex = 40;
            this.lblModel.Text = "Model:";
            // 
            // lblBrand
            // 
            this.lblBrand.AutoSize = true;
            this.lblBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblBrand.Location = new System.Drawing.Point(5, 30);
            this.lblBrand.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBrand.Name = "lblBrand";
            this.lblBrand.Size = new System.Drawing.Size(56, 20);
            this.lblBrand.TabIndex = 38;
            this.lblBrand.Text = "Brand:";
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.Color.Transparent;
            this.btnsave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnsave.FlatAppearance.BorderSize = 0;
            this.btnsave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnsave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnsave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnsave.ForeColor = System.Drawing.Color.Transparent;
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.Location = new System.Drawing.Point(809, 326);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(60, 60);
            this.btnsave.TabIndex = 21;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // grpbxrenting
            // 
            this.grpbxrenting.Controls.Add(this.btnTotalPrice);
            this.grpbxrenting.Controls.Add(this.lblDays);
            this.grpbxrenting.Controls.Add(this.lblTotalPrice);
            this.grpbxrenting.Controls.Add(this.lblReceiving);
            this.grpbxrenting.Controls.Add(this.dateTimeDue);
            this.grpbxrenting.Controls.Add(this.dateTimeReceiving);
            this.grpbxrenting.Controls.Add(this.lblDue);
            this.grpbxrenting.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.grpbxrenting.Location = new System.Drawing.Point(602, 12);
            this.grpbxrenting.Name = "grpbxrenting";
            this.grpbxrenting.Size = new System.Drawing.Size(312, 245);
            this.grpbxrenting.TabIndex = 3;
            this.grpbxrenting.TabStop = false;
            this.grpbxrenting.Text = "Renting Information";
            // 
            // btnTotalPrice
            // 
            this.btnTotalPrice.BackColor = System.Drawing.Color.Transparent;
            this.btnTotalPrice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTotalPrice.FlatAppearance.BorderSize = 0;
            this.btnTotalPrice.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnTotalPrice.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnTotalPrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTotalPrice.ForeColor = System.Drawing.Color.Transparent;
            this.btnTotalPrice.Image = ((System.Drawing.Image)(resources.GetObject("btnTotalPrice.Image")));
            this.btnTotalPrice.Location = new System.Drawing.Point(207, 155);
            this.btnTotalPrice.Margin = new System.Windows.Forms.Padding(2);
            this.btnTotalPrice.Name = "btnTotalPrice";
            this.btnTotalPrice.Size = new System.Drawing.Size(90, 90);
            this.btnTotalPrice.TabIndex = 20;
            this.btnTotalPrice.UseVisualStyleBackColor = false;
            this.btnTotalPrice.Click += new System.EventHandler(this.btnTotalPrice_Click);
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblDays.Location = new System.Drawing.Point(46, 122);
            this.lblDays.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(62, 25);
            this.lblDays.TabIndex = 50;
            this.lblDays.Text = "days";
            this.lblDays.Visible = false;
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.AutoSize = true;
            this.lblTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTotalPrice.Location = new System.Drawing.Point(46, 169);
            this.lblTotalPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(64, 25);
            this.lblTotalPrice.TabIndex = 49;
            this.lblTotalPrice.Text = "price";
            this.lblTotalPrice.Visible = false;
            // 
            // lblReceiving
            // 
            this.lblReceiving.AutoSize = true;
            this.lblReceiving.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblReceiving.Location = new System.Drawing.Point(5, 37);
            this.lblReceiving.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReceiving.Name = "lblReceiving";
            this.lblReceiving.Size = new System.Drawing.Size(121, 20);
            this.lblReceiving.TabIndex = 46;
            this.lblReceiving.Text = "Receiving Date:";
            // 
            // dateTimeDue
            // 
            this.dateTimeDue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dateTimeDue.Location = new System.Drawing.Point(130, 72);
            this.dateTimeDue.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimeDue.Name = "dateTimeDue";
            this.dateTimeDue.Size = new System.Drawing.Size(177, 26);
            this.dateTimeDue.TabIndex = 19;
            // 
            // dateTimeReceiving
            // 
            this.dateTimeReceiving.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dateTimeReceiving.Location = new System.Drawing.Point(130, 32);
            this.dateTimeReceiving.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimeReceiving.Name = "dateTimeReceiving";
            this.dateTimeReceiving.Size = new System.Drawing.Size(177, 26);
            this.dateTimeReceiving.TabIndex = 18;
            // 
            // lblDue
            // 
            this.lblDue.AutoSize = true;
            this.lblDue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblDue.Location = new System.Drawing.Point(5, 77);
            this.lblDue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDue.Name = "lblDue";
            this.lblDue.Size = new System.Drawing.Size(82, 20);
            this.lblDue.TabIndex = 47;
            this.lblDue.Text = "Due Date:";
            // 
            // AddCustomer
            // 
            this.AcceptButton = this.btnsave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 432);
            this.Controls.Add(this.grpbxrenting);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.grpbxcar);
            this.Controls.Add(this.grpbxpersonal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Customer";
            this.grpbxpersonal.ResumeLayout(false);
            this.grpbxpersonal.PerformLayout();
            this.grpbxcar.ResumeLayout(false);
            this.grpbxcar.PerformLayout();
            this.grpbxrenting.ResumeLayout(false);
            this.grpbxrenting.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpbxpersonal;
        protected System.Windows.Forms.ComboBox comboGender;
        private System.Windows.Forms.DateTimePicker dateTimeBirth;
        private System.Windows.Forms.TextBox txtIssue;
        private System.Windows.Forms.TextBox txtLicence;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtCell;
        private System.Windows.Forms.TextBox txtPlace;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label lblIssue;
        private System.Windows.Forms.Label lblLicence;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblCell;
        private System.Windows.Forms.Label lblPlace;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.GroupBox grpbxcar;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.Label lblFuel;
        private System.Windows.Forms.Label lblTransmission;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblBrand;
        private System.Windows.Forms.ComboBox comboTransmission;
        private System.Windows.Forms.ComboBox comboFuel;
        private System.Windows.Forms.ComboBox comboColor;
        private System.Windows.Forms.ComboBox comboBrand;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.ComboBox comboModel;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblDailyPrice;
        private System.Windows.Forms.Button dailyPrice;
        private System.Windows.Forms.GroupBox grpbxrenting;
        private System.Windows.Forms.Button btnTotalPrice;
        private System.Windows.Forms.Label lblDays;
        private System.Windows.Forms.Label lblTotalPrice;
        private System.Windows.Forms.Label lblReceiving;
        private System.Windows.Forms.DateTimePicker dateTimeDue;
        private System.Windows.Forms.DateTimePicker dateTimeReceiving;
        private System.Windows.Forms.Label lblDue;
    }
}

