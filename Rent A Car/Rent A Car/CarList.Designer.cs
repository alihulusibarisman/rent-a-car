﻿namespace Rent_A_Car
{
    partial class CarList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.gbAddCar = new System.Windows.Forms.GroupBox();
            this.cmbxcolor = new System.Windows.Forms.ComboBox();
            this.cmbxtransmission = new System.Windows.Forms.ComboBox();
            this.cmbxfuel = new System.Windows.Forms.ComboBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.txtyear = new System.Windows.Forms.TextBox();
            this.txtmodel = new System.Windows.Forms.TextBox();
            this.txtbrand = new System.Windows.Forms.TextBox();
            this.txtnumberplate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtsearch = new System.Windows.Forms.TextBox();
            this.gbUpdateCar = new System.Windows.Forms.GroupBox();
            this.cmbxcolorgb2 = new System.Windows.Forms.ComboBox();
            this.cmbxtransmissiongb2 = new System.Windows.Forms.ComboBox();
            this.cmbxfuelgb2 = new System.Windows.Forms.ComboBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtpricegb2 = new System.Windows.Forms.TextBox();
            this.txtyeargb2 = new System.Windows.Forms.TextBox();
            this.txtmodelgb2 = new System.Windows.Forms.TextBox();
            this.txtbrandgb2 = new System.Windows.Forms.TextBox();
            this.txtnumberplategb2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.gbAddCar.SuspendLayout();
            this.gbUpdateCar.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.updateToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(748, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.addToolStripMenuItem.Text = "ADD";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            this.updateToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.updateToolStripMenuItem.Text = "UPDATE";
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.deleteToolStripMenuItem.Text = "DELETE";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 74);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(716, 231);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEnter);
            // 
            // gbAddCar
            // 
            this.gbAddCar.Controls.Add(this.cmbxcolor);
            this.gbAddCar.Controls.Add(this.cmbxtransmission);
            this.gbAddCar.Controls.Add(this.cmbxfuel);
            this.gbAddCar.Controls.Add(this.btnAdd);
            this.gbAddCar.Controls.Add(this.txtprice);
            this.gbAddCar.Controls.Add(this.txtyear);
            this.gbAddCar.Controls.Add(this.txtmodel);
            this.gbAddCar.Controls.Add(this.txtbrand);
            this.gbAddCar.Controls.Add(this.txtnumberplate);
            this.gbAddCar.Controls.Add(this.label9);
            this.gbAddCar.Controls.Add(this.label8);
            this.gbAddCar.Controls.Add(this.label7);
            this.gbAddCar.Controls.Add(this.label6);
            this.gbAddCar.Controls.Add(this.label5);
            this.gbAddCar.Controls.Add(this.label4);
            this.gbAddCar.Controls.Add(this.label3);
            this.gbAddCar.Controls.Add(this.label2);
            this.gbAddCar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbAddCar.Location = new System.Drawing.Point(11, 317);
            this.gbAddCar.Margin = new System.Windows.Forms.Padding(2);
            this.gbAddCar.Name = "gbAddCar";
            this.gbAddCar.Padding = new System.Windows.Forms.Padding(2);
            this.gbAddCar.Size = new System.Drawing.Size(717, 162);
            this.gbAddCar.TabIndex = 5;
            this.gbAddCar.TabStop = false;
            this.gbAddCar.Text = "Add Car";
            this.gbAddCar.Visible = false;
            // 
            // cmbxcolor
            // 
            this.cmbxcolor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxcolor.FormattingEnabled = true;
            this.cmbxcolor.Items.AddRange(new object[] {
            "White",
            "Black",
            "Metalic Grey",
            "Blue",
            "Red",
            "Yellow",
            "Orange",
            "Green"});
            this.cmbxcolor.Location = new System.Drawing.Point(545, 60);
            this.cmbxcolor.Name = "cmbxcolor";
            this.cmbxcolor.Size = new System.Drawing.Size(91, 26);
            this.cmbxcolor.TabIndex = 19;
            // 
            // cmbxtransmission
            // 
            this.cmbxtransmission.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxtransmission.FormattingEnabled = true;
            this.cmbxtransmission.Items.AddRange(new object[] {
            "Automatic",
            "Manuel"});
            this.cmbxtransmission.Location = new System.Drawing.Point(347, 60);
            this.cmbxtransmission.Name = "cmbxtransmission";
            this.cmbxtransmission.Size = new System.Drawing.Size(95, 26);
            this.cmbxtransmission.TabIndex = 18;
            // 
            // cmbxfuel
            // 
            this.cmbxfuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxfuel.FormattingEnabled = true;
            this.cmbxfuel.Items.AddRange(new object[] {
            "Diesel",
            "Gasoline"});
            this.cmbxfuel.Location = new System.Drawing.Point(452, 60);
            this.cmbxfuel.Name = "cmbxfuel";
            this.cmbxfuel.Size = new System.Drawing.Size(82, 26);
            this.cmbxfuel.TabIndex = 17;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAdd.Location = new System.Drawing.Point(307, 102);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(91, 49);
            this.btnAdd.TabIndex = 16;
            this.btnAdd.Text = "ADD CAR";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtprice
            // 
            this.txtprice.Location = new System.Drawing.Point(649, 60);
            this.txtprice.Margin = new System.Windows.Forms.Padding(2);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(64, 26);
            this.txtprice.TabIndex = 15;
            // 
            // txtyear
            // 
            this.txtyear.Location = new System.Drawing.Point(275, 60);
            this.txtyear.Margin = new System.Windows.Forms.Padding(2);
            this.txtyear.Name = "txtyear";
            this.txtyear.Size = new System.Drawing.Size(66, 26);
            this.txtyear.TabIndex = 11;
            // 
            // txtmodel
            // 
            this.txtmodel.Location = new System.Drawing.Point(195, 60);
            this.txtmodel.Margin = new System.Windows.Forms.Padding(2);
            this.txtmodel.Name = "txtmodel";
            this.txtmodel.Size = new System.Drawing.Size(68, 26);
            this.txtmodel.TabIndex = 10;
            // 
            // txtbrand
            // 
            this.txtbrand.Location = new System.Drawing.Point(111, 60);
            this.txtbrand.Margin = new System.Windows.Forms.Padding(2);
            this.txtbrand.Name = "txtbrand";
            this.txtbrand.Size = new System.Drawing.Size(76, 26);
            this.txtbrand.TabIndex = 9;
            // 
            // txtnumberplate
            // 
            this.txtnumberplate.Location = new System.Drawing.Point(4, 60);
            this.txtnumberplate.Margin = new System.Windows.Forms.Padding(2);
            this.txtnumberplate.Name = "txtnumberplate";
            this.txtnumberplate.Size = new System.Drawing.Size(100, 26);
            this.txtnumberplate.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label9.Location = new System.Drawing.Point(661, 34);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 17);
            this.label9.TabIndex = 7;
            this.label9.Text = "Price";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label8.Location = new System.Drawing.Point(566, 34);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 17);
            this.label8.TabIndex = 6;
            this.label8.Text = "Color";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.Location = new System.Drawing.Point(473, 34);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "Fuel";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(350, 34);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "Transmission";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(289, 34);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "Year";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(205, 34);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Model";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(127, 34);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Brand";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(7, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Number Plate";
            // 
            // txtsearch
            // 
            this.txtsearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtsearch.Location = new System.Drawing.Point(12, 39);
            this.txtsearch.Name = "txtsearch";
            this.txtsearch.Size = new System.Drawing.Size(159, 26);
            this.txtsearch.TabIndex = 6;
            this.txtsearch.TextChanged += new System.EventHandler(this.txtsearch_TextChanged);
            // 
            // gbUpdateCar
            // 
            this.gbUpdateCar.Controls.Add(this.cmbxcolorgb2);
            this.gbUpdateCar.Controls.Add(this.cmbxtransmissiongb2);
            this.gbUpdateCar.Controls.Add(this.cmbxfuelgb2);
            this.gbUpdateCar.Controls.Add(this.btnUpdate);
            this.gbUpdateCar.Controls.Add(this.txtpricegb2);
            this.gbUpdateCar.Controls.Add(this.txtyeargb2);
            this.gbUpdateCar.Controls.Add(this.txtmodelgb2);
            this.gbUpdateCar.Controls.Add(this.txtbrandgb2);
            this.gbUpdateCar.Controls.Add(this.txtnumberplategb2);
            this.gbUpdateCar.Controls.Add(this.label1);
            this.gbUpdateCar.Controls.Add(this.label10);
            this.gbUpdateCar.Controls.Add(this.label11);
            this.gbUpdateCar.Controls.Add(this.label12);
            this.gbUpdateCar.Controls.Add(this.label13);
            this.gbUpdateCar.Controls.Add(this.label14);
            this.gbUpdateCar.Controls.Add(this.label15);
            this.gbUpdateCar.Controls.Add(this.label16);
            this.gbUpdateCar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbUpdateCar.Location = new System.Drawing.Point(11, 317);
            this.gbUpdateCar.Margin = new System.Windows.Forms.Padding(2);
            this.gbUpdateCar.Name = "gbUpdateCar";
            this.gbUpdateCar.Padding = new System.Windows.Forms.Padding(2);
            this.gbUpdateCar.Size = new System.Drawing.Size(717, 162);
            this.gbUpdateCar.TabIndex = 20;
            this.gbUpdateCar.TabStop = false;
            this.gbUpdateCar.Text = "Update Car";
            this.gbUpdateCar.Visible = false;
            // 
            // cmbxcolorgb2
            // 
            this.cmbxcolorgb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxcolorgb2.FormattingEnabled = true;
            this.cmbxcolorgb2.Items.AddRange(new object[] {
            "White",
            "Black",
            "Metalic Grey",
            "Blue",
            "Red",
            "Yellow",
            "Orange",
            "Green"});
            this.cmbxcolorgb2.Location = new System.Drawing.Point(545, 60);
            this.cmbxcolorgb2.Name = "cmbxcolorgb2";
            this.cmbxcolorgb2.Size = new System.Drawing.Size(91, 26);
            this.cmbxcolorgb2.TabIndex = 19;
            // 
            // cmbxtransmissiongb2
            // 
            this.cmbxtransmissiongb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxtransmissiongb2.FormattingEnabled = true;
            this.cmbxtransmissiongb2.Items.AddRange(new object[] {
            "Automatic",
            "Manuel"});
            this.cmbxtransmissiongb2.Location = new System.Drawing.Point(347, 60);
            this.cmbxtransmissiongb2.Name = "cmbxtransmissiongb2";
            this.cmbxtransmissiongb2.Size = new System.Drawing.Size(95, 26);
            this.cmbxtransmissiongb2.TabIndex = 18;
            // 
            // cmbxfuelgb2
            // 
            this.cmbxfuelgb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxfuelgb2.FormattingEnabled = true;
            this.cmbxfuelgb2.Items.AddRange(new object[] {
            "Diesel",
            "Gasoline"});
            this.cmbxfuelgb2.Location = new System.Drawing.Point(452, 60);
            this.cmbxfuelgb2.Name = "cmbxfuelgb2";
            this.cmbxfuelgb2.Size = new System.Drawing.Size(82, 26);
            this.cmbxfuelgb2.TabIndex = 17;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnUpdate.Location = new System.Drawing.Point(307, 102);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(91, 49);
            this.btnUpdate.TabIndex = 16;
            this.btnUpdate.Text = "UPDATE CAR";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // txtpricegb2
            // 
            this.txtpricegb2.Location = new System.Drawing.Point(647, 60);
            this.txtpricegb2.Margin = new System.Windows.Forms.Padding(2);
            this.txtpricegb2.Name = "txtpricegb2";
            this.txtpricegb2.Size = new System.Drawing.Size(64, 26);
            this.txtpricegb2.TabIndex = 15;
            // 
            // txtyeargb2
            // 
            this.txtyeargb2.Location = new System.Drawing.Point(275, 60);
            this.txtyeargb2.Margin = new System.Windows.Forms.Padding(2);
            this.txtyeargb2.Name = "txtyeargb2";
            this.txtyeargb2.Size = new System.Drawing.Size(66, 26);
            this.txtyeargb2.TabIndex = 11;
            // 
            // txtmodelgb2
            // 
            this.txtmodelgb2.Location = new System.Drawing.Point(195, 60);
            this.txtmodelgb2.Margin = new System.Windows.Forms.Padding(2);
            this.txtmodelgb2.Name = "txtmodelgb2";
            this.txtmodelgb2.Size = new System.Drawing.Size(68, 26);
            this.txtmodelgb2.TabIndex = 10;
            // 
            // txtbrandgb2
            // 
            this.txtbrandgb2.Location = new System.Drawing.Point(111, 60);
            this.txtbrandgb2.Margin = new System.Windows.Forms.Padding(2);
            this.txtbrandgb2.Name = "txtbrandgb2";
            this.txtbrandgb2.Size = new System.Drawing.Size(76, 26);
            this.txtbrandgb2.TabIndex = 9;
            // 
            // txtnumberplategb2
            // 
            this.txtnumberplategb2.Location = new System.Drawing.Point(4, 60);
            this.txtnumberplategb2.Margin = new System.Windows.Forms.Padding(2);
            this.txtnumberplategb2.Name = "txtnumberplategb2";
            this.txtnumberplategb2.Size = new System.Drawing.Size(100, 26);
            this.txtnumberplategb2.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(660, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Price";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.Location = new System.Drawing.Point(566, 34);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 17);
            this.label10.TabIndex = 6;
            this.label10.Text = "Color";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label11.Location = new System.Drawing.Point(473, 34);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 17);
            this.label11.TabIndex = 5;
            this.label11.Text = "Fuel";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label12.Location = new System.Drawing.Point(348, 34);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 17);
            this.label12.TabIndex = 4;
            this.label12.Text = "Transmission";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label13.Location = new System.Drawing.Point(289, 34);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 17);
            this.label13.TabIndex = 3;
            this.label13.Text = "Year";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label14.Location = new System.Drawing.Point(205, 34);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 17);
            this.label14.TabIndex = 2;
            this.label14.Text = "Model";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label15.Location = new System.Drawing.Point(127, 34);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 17);
            this.label15.TabIndex = 1;
            this.label15.Text = "Brand";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label16.Location = new System.Drawing.Point(7, 34);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(94, 17);
            this.label16.TabIndex = 0;
            this.label16.Text = "Number Plate";
            // 
            // CarList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 490);
            this.Controls.Add(this.gbUpdateCar);
            this.Controls.Add(this.txtsearch);
            this.Controls.Add(this.gbAddCar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "CarList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Car List";
            this.Load += new System.EventHandler(this.CarList_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.gbAddCar.ResumeLayout(false);
            this.gbAddCar.PerformLayout();
            this.gbUpdateCar.ResumeLayout(false);
            this.gbUpdateCar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox gbAddCar;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.TextBox txtyear;
        private System.Windows.Forms.TextBox txtmodel;
        private System.Windows.Forms.TextBox txtbrand;
        private System.Windows.Forms.TextBox txtnumberplate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbxtransmission;
        private System.Windows.Forms.ComboBox cmbxfuel;
        private System.Windows.Forms.ComboBox cmbxcolor;
        private System.Windows.Forms.TextBox txtsearch;
        private System.Windows.Forms.GroupBox gbUpdateCar;
        private System.Windows.Forms.ComboBox cmbxcolorgb2;
        private System.Windows.Forms.ComboBox cmbxtransmissiongb2;
        private System.Windows.Forms.ComboBox cmbxfuelgb2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtpricegb2;
        private System.Windows.Forms.TextBox txtyeargb2;
        private System.Windows.Forms.TextBox txtmodelgb2;
        private System.Windows.Forms.TextBox txtbrandgb2;
        private System.Windows.Forms.TextBox txtnumberplategb2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
    }
}