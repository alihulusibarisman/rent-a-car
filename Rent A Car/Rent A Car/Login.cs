﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rent_A_Car
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            LoadCaptcha();
        }

        OleDbConnection con;
        OleDbCommand cmd;
        OleDbDataReader dr;
        
        String num = "";
        private void LoadCaptcha()
        {
            Random r = new Random();
            num = r.Next(10) + "";
            num += (char)r.Next('A', 'Z');
            num += r.Next(10) + "";
            num += (char)r.Next('A', 'Z');

            //Image img = new Image();
            Bitmap bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height); //137,74
            Font f = new Font("Calibri", 45, FontStyle.Bold | FontStyle.Italic | FontStyle.Underline | FontStyle.Strikeout, GraphicsUnit.Pixel);
            Graphics g = Graphics.FromImage(bmp);
            g.DrawString(num + "", f, Brushes.Green, new PointF(0, 0));

            for (int i = 0; i < 20; i++)
            {
                g.DrawEllipse(new Pen(Color.Black, 5), r.Next(pictureBox1.Width), r.Next(pictureBox1.Height), 5, 5);
            }
            
            pictureBox1.Image = bmp;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LoadCaptcha();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtCaptcha.Text.ToLower() != num.ToString().ToLower())
            {
                MessageBox.Show("Captcha does not match.");
                LoadCaptcha();
                txtCaptcha.Text = "";
                return;
            }

            string username = txtUser.Text;
            string password = txtPassword.Text;
            con = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
            cmd = new OleDbCommand();
            con.Open();
            cmd.Connection = con;
            cmd.CommandText = "SELECT * FROM user_information where name='" + txtUser.Text + "' AND password='" + txtPassword.Text + "'";
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                Homepage f2 = new Homepage();
                f2.Show();
                this.Hide();

            }
            else
            {
                MessageBox.Show("User Name or Password Wrong!");
            }
            con.Close();

        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
