﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rent_A_Car
{
    public partial class CarList : Form
    {
        OleDbConnection Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
        OleDbCommand Command = new OleDbCommand();
        OleDbDataAdapter adtr = new OleDbDataAdapter();
        DataSet ds = new System.Data.DataSet();
        public CarList()
        {
            InitializeComponent();
        }

        void list()
        {
            Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
            adtr = new OleDbDataAdapter("Select *from car_list", Connection);
            ds = new DataSet();
            Connection.Open();
            adtr.Fill(ds, "car_list");
            dataGridView1.DataSource = ds.Tables["car_list"];
            Connection.Close();
        }

        private void CarList_Load(object sender, EventArgs e)
        {
            list();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtnumberplate.Text != "" && txtbrand.Text != "" && txtmodel.Text != "" &&  txtyear.Text != "" &&
                cmbxtransmission.SelectedItem.ToString() != "" && cmbxfuel.SelectedItem.ToString() != "" && cmbxcolor.Text != "" &&
                txtprice.Text != "")
            {
                Connection.Open();
                Command.Connection = Connection;
                Command.CommandText = "Insert Into car_list(car_numberplate,car_brand,car_model,car_year,transmission,fuel,color,price) Values ('" +
                    txtnumberplate.Text + "','" + txtbrand.Text + "','" + txtmodel.Text + "','" + txtyear.Text + "','" +
                    cmbxtransmission.SelectedItem.ToString() + "','" + cmbxfuel.SelectedItem.ToString() + "','" + cmbxcolor.Text + "','" +
                    txtprice.Text + "')";
                Command.ExecuteNonQuery();
                Command.Dispose();
                Connection.Close();
                MessageBox.Show("Registration Successful.", "Registration Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Connection.Close();
                list();
            }
            else
            {
                MessageBox.Show("Please fill in the blanks!");
            }
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbAddCar.Visible = true;
            gbUpdateCar.Visible = false;
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbUpdateCar.Visible = true;
            gbAddCar.Visible = false;
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            txtnumberplategb2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            txtbrandgb2.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            txtmodelgb2.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            txtyeargb2.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            cmbxtransmissiongb2.SelectedItem = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            cmbxfuelgb2.SelectedItem = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            cmbxcolorgb2.SelectedItem = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            txtpricegb2.Text = dataGridView1.CurrentRow.Cells[8].Value.ToString();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure that you want to delete ?", "Warning", MessageBoxButtons.YesNo,
              MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Connection.Open();
                Command.Connection = Connection;
                Command.CommandText = "DELETE FROM car_list WHERE car_numberplate ='" + dataGridView1.CurrentRow.Cells[1].Value.ToString() + "'";
                Command.ExecuteNonQuery();
                Connection.Close();
                MessageBox.Show("Delete is completed.");
                list();
            }   
        }

        private void txtsearch_TextChanged(object sender, EventArgs e)
        {
            Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
            adtr = new OleDbDataAdapter("Select *from car_list where car_numberplate like '" + txtsearch.Text + "%'", Connection);
            ds = new DataSet();
            Connection.Open();
            adtr.Fill(ds, "car_list");
            dataGridView1.DataSource = ds.Tables["car_list"];
            Connection.Close();
        }

        

        

        
    }
}
