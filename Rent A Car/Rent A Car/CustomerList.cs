﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rent_A_Car
{
    public partial class CustomerList : Form
    {
        OleDbConnection Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
        OleDbCommand Command = new OleDbCommand();
        OleDbDataAdapter adtr = new OleDbDataAdapter();
        DataSet ds = new System.Data.DataSet();

        public CustomerList()
        {
            InitializeComponent();
        }

        void list()
        {
            Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
            adtr = new OleDbDataAdapter("Select *from customer_list", Connection);
            ds = new DataSet();
            Connection.Open();
            adtr.Fill(ds, "customer_list");
            GW1.DataSource = ds.Tables["customer_list"];
            Connection.Close();
        }

        private void CustomerList_Load(object sender, EventArgs e)
        {
            list();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
           Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
           adtr = new OleDbDataAdapter("Select *from customer_list where customer_name like '" + txtSearch.Text + "%'", Connection);
           ds = new DataSet();
           Connection.Open();
           adtr.Fill(ds, "customer_list");
           GW1.DataSource = ds.Tables["customer_list"];
           Connection.Close();
        }


    }
}
