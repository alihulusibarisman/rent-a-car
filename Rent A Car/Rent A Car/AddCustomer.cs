﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rent_A_Car
{
    public partial class AddCustomer : Form
    {
        OleDbConnection Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
        OleDbCommand Command = new OleDbCommand();
        OleDbDataAdapter adtr = new OleDbDataAdapter();
        DataSet ds = new System.Data.DataSet();
        public AddCustomer()
        {
            InitializeComponent();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtId.Text != "" && txtName.Text != "" && txtSurname.Text != "" && comboGender.SelectedItem.ToString() != "" &&
               txtPlace.Text != "" && dateTimeBirth.Text != "" && txtCell.Text != "" && txtEmail.Text != "" && txtAddress.Text != ""
               && txtLicence.Text != "" && txtIssue.Text != "" && comboBrand.SelectedItem.ToString() != "" && comboModel.SelectedItem.ToString() != ""
               && comboTransmission.SelectedItem.ToString() != "" && comboFuel.SelectedItem.ToString() != "" && comboColor.SelectedItem.ToString() != "")
            {
                Connection.Open();
                Command.Connection = Connection;
                Command.CommandText = "Insert Into customer_list(customer_id,customer_name,customer_surname,gender,placeofbirth,dateofbirth,cellphone,email,address,licence_id,issue,brand,model,transmission,fuel,color,price) Values ('" +
                    txtId.Text + "','" + txtName.Text + "','" + txtSurname.Text + "','" + comboGender.SelectedItem.ToString() + "','" + txtPlace.Text + "','" + dateTimeBirth.Text + "','" +
                    txtCell.Text + "','" + txtEmail.Text + "','" + txtAddress.Text + "','"+ txtLicence.Text + "','"+ txtIssue.Text + "','" +
                    comboBrand.SelectedItem.ToString() + "','" + comboModel.SelectedItem.ToString() + "','" + comboTransmission.SelectedItem.ToString() + "','" +
                    comboFuel.SelectedItem.ToString() + "','" + comboColor.SelectedItem.ToString() + "')";
                Command.ExecuteNonQuery();
                Command.Dispose();
                Connection.Close();
                MessageBox.Show("Registration Successful!");
                Connection.Close();
            }
            else
            {
                MessageBox.Show("Please fill in the blanks!");
            }
        
        }

        private void comboBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ADDING ARRAYLISTS
            ArrayList bmw = new ArrayList();
            ArrayList audi = new ArrayList();
            ArrayList mercedes = new ArrayList();
            ArrayList renault = new ArrayList();
            ArrayList volkswagen = new ArrayList();
            ArrayList skoda = new ArrayList();
            ArrayList seat = new ArrayList();
            ArrayList porsche = new ArrayList();
            ArrayList volvo = new ArrayList();
            ArrayList nissan = new ArrayList();
            ArrayList toyota = new ArrayList();

            //ADDING STRING ARRAY
            String[] bmw1 = new String[22] {"116d","116i","120d","218i","220d","216d","315","316","320Ci","320Cd","316ti","428i","418i",
                "520d","520","525td","i8","M3","M5","Z4","X5","X6"};
            String[] audi1 = new String[15] { "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "TT", "Q7", "Q3", "Q5", "RS5", "RS6", "RS7" };
            String[] mercedes1 = new String[11] { "CLA180", "A180","A200","E320","E180","E250","CLK500","CLK200","Vito","Viano",
                "Sprinter"};
            String[] renault1 = new String[5] { "MEGANE", "CLIO", "FLUENCE", "TALISMAN", "SYMBOL" };
            String[] volkswagen1 = new String[14] { "GOLF", "JETTA", "POLO", "PASSAT", "NEW BEETLE" ,"CARAVELLE", "CADDY", "CRAFTER",
                "TRANSPORTER", "TIGUAN", "TOUAREG", "SCIROCCO", "CC", "AMAROK"};
            String[] skoda1 = new String[5] { "Fabia", "Superb", "Rapid", "Octavia", "Yeti" };
            String[] seat1 = new String[4] { "LEON", "IBIZA", "TOLEDO", "ATECA" };
            String[] porsche1 = new String[2] { "CAYENNE", "PANAMERA" };
            String[] volvo1 = new String[5] { "XC90", "V40", "S60", "V60", "S80" };
            String[] nissan1 = new String[5] { "XTRAİL", "QASHQAI", "JUKE", "NAVARA", "MICRA" };
            String[] toyota1 = new String[4] { "COROLLA", "AURIS", "AVENSIS", "YARIS" };

            //ADDING ARRAYS İN TO ARRAYLISTS
            bmw.AddRange(bmw1);
            audi.AddRange(audi1);
            mercedes.AddRange(mercedes1);
            renault.AddRange(renault1);
            volkswagen.AddRange(volkswagen1);
            skoda.AddRange(skoda1);
            seat.AddRange(seat1);
            porsche.AddRange(porsche1);
            volvo.AddRange(volvo1);
            nissan.AddRange(nissan1);
            toyota.AddRange(toyota1);

            //SELECTED ITEMS COMBOBOX ---> LİSTBOX
            if ("BMW".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in bmw)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("AUDI".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in audi)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("MERCEDES".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in mercedes)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("RENAULT".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in renault)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("VOLKSWAGEN".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in volkswagen)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("SKODA".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in skoda)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("SEAT".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in seat)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("PORSCHE".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in porsche)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("VOLVO".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in volvo)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("NISSAN".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in nissan)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("TOYOTA".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in toyota)
                {
                    comboModel.Items.Add(car);
                }
            }
        }

        private void txtId_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
                && !char.IsSeparator(e.KeyChar);
        }

        private void txtSurname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
                && !char.IsSeparator(e.KeyChar);
        }

        private void txtPlace_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
                && !char.IsSeparator(e.KeyChar);
        }

        private void txtCell_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtLicence_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        
        private void txtIssue_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

    }
}
