﻿namespace Rent_A_Car
{
    partial class Homepage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Homepage));
            this.btnExit = new System.Windows.Forms.Button();
            this.btnContract = new System.Windows.Forms.Button();
            this.btnCarList = new System.Windows.Forms.Button();
            this.btnCustomerList = new System.Windows.Forms.Button();
            this.btnAddCustomer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.ForeColor = System.Drawing.Color.Transparent;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.Location = new System.Drawing.Point(408, 307);
            this.btnExit.Margin = new System.Windows.Forms.Padding(2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(80, 80);
            this.btnExit.TabIndex = 9;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnContract
            // 
            this.btnContract.BackColor = System.Drawing.Color.Transparent;
            this.btnContract.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnContract.FlatAppearance.BorderSize = 0;
            this.btnContract.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnContract.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnContract.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContract.ForeColor = System.Drawing.Color.Transparent;
            this.btnContract.Image = ((System.Drawing.Image)(resources.GetObject("btnContract.Image")));
            this.btnContract.Location = new System.Drawing.Point(269, 190);
            this.btnContract.Margin = new System.Windows.Forms.Padding(2);
            this.btnContract.Name = "btnContract";
            this.btnContract.Size = new System.Drawing.Size(135, 135);
            this.btnContract.TabIndex = 8;
            this.btnContract.UseVisualStyleBackColor = false;
            this.btnContract.Click += new System.EventHandler(this.btnContract_Click);
            // 
            // btnCarList
            // 
            this.btnCarList.BackColor = System.Drawing.Color.Transparent;
            this.btnCarList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCarList.FlatAppearance.BorderSize = 0;
            this.btnCarList.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnCarList.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCarList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCarList.ForeColor = System.Drawing.Color.Transparent;
            this.btnCarList.Image = ((System.Drawing.Image)(resources.GetObject("btnCarList.Image")));
            this.btnCarList.Location = new System.Drawing.Point(64, 195);
            this.btnCarList.Margin = new System.Windows.Forms.Padding(2);
            this.btnCarList.Name = "btnCarList";
            this.btnCarList.Size = new System.Drawing.Size(125, 125);
            this.btnCarList.TabIndex = 7;
            this.btnCarList.UseVisualStyleBackColor = false;
            this.btnCarList.Click += new System.EventHandler(this.btnCarList_Click);
            // 
            // btnCustomerList
            // 
            this.btnCustomerList.BackColor = System.Drawing.Color.Transparent;
            this.btnCustomerList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCustomerList.FlatAppearance.BorderSize = 0;
            this.btnCustomerList.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnCustomerList.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCustomerList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomerList.ForeColor = System.Drawing.Color.Transparent;
            this.btnCustomerList.Image = ((System.Drawing.Image)(resources.GetObject("btnCustomerList.Image")));
            this.btnCustomerList.Location = new System.Drawing.Point(269, 30);
            this.btnCustomerList.Margin = new System.Windows.Forms.Padding(2);
            this.btnCustomerList.Name = "btnCustomerList";
            this.btnCustomerList.Size = new System.Drawing.Size(135, 135);
            this.btnCustomerList.TabIndex = 6;
            this.btnCustomerList.UseVisualStyleBackColor = false;
            this.btnCustomerList.Click += new System.EventHandler(this.btnCustomerList_Click);
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.BackColor = System.Drawing.Color.Transparent;
            this.btnAddCustomer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddCustomer.FlatAppearance.BorderSize = 0;
            this.btnAddCustomer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnAddCustomer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnAddCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCustomer.ForeColor = System.Drawing.Color.Transparent;
            this.btnAddCustomer.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCustomer.Image")));
            this.btnAddCustomer.Location = new System.Drawing.Point(54, 30);
            this.btnAddCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(135, 135);
            this.btnAddCustomer.TabIndex = 5;
            this.btnAddCustomer.UseVisualStyleBackColor = false;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // Homepage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 409);
            this.ControlBox = false;
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnContract);
            this.Controls.Add(this.btnCarList);
            this.Controls.Add(this.btnCustomerList);
            this.Controls.Add(this.btnAddCustomer);
            this.Name = "Homepage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rent A Car";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnContract;
        private System.Windows.Forms.Button btnCarList;
        private System.Windows.Forms.Button btnCustomerList;
        private System.Windows.Forms.Button btnAddCustomer;
    }
}