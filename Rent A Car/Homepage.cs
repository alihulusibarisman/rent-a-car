﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rent_A_Car
{
    public partial class Homepage : Form
    {
        public Homepage()
        {
            InitializeComponent();
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            AddCustomer f1 = new AddCustomer();
            f1.Show();
        }

        private void btnCustomerList_Click(object sender, EventArgs e)
        {
            CustomerList f2 = new CustomerList();
            f2.Show();
        }

        private void btnCarList_Click(object sender, EventArgs e)
        {
            CarList f3 = new CarList();
            f3.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
            Login f5 = new Login();
            f5.Show();
        }

       
    }
}
