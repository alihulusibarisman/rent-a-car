﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rent_A_Car
{
    public partial class CustomerList : Form
    {
        OleDbConnection Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
        OleDbCommand Command = new OleDbCommand();
        OleDbDataAdapter adtr = new OleDbDataAdapter();
        DataSet ds = new System.Data.DataSet();

        public CustomerList()
        {
            InitializeComponent();
            SelectionSortBrand();
            SelectionSortColor();
        }

        void list()
        {
            Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
            adtr = new OleDbDataAdapter("Select *from customer_list", Connection);
            ds = new DataSet();
            Connection.Open();
            adtr.Fill(ds, "customer_list");
            GW1.DataSource = ds.Tables["customer_list"];
            Connection.Close();
        }

        private void CustomerList_Load(object sender, EventArgs e)
        {
            list();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
           Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
           adtr = new OleDbDataAdapter("Select *from customer_list where customer_name like '" + txtSearch.Text + "%'", Connection);
           ds = new DataSet();
           Connection.Open();
           adtr.Fill(ds, "customer_list");
           GW1.DataSource = ds.Tables["customer_list"];
           Connection.Close();
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure that you want to delete ?", "Warning", MessageBoxButtons.YesNo,
              MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Connection.Open();
                Command.Connection = Connection;
                Command.CommandText = "DELETE FROM customer_list WHERE customer_id='" + GW1.CurrentRow.Cells[1].Value.ToString() + "'";
                Command.ExecuteNonQuery();
                Connection.Close();
                MessageBox.Show("Deleting Successful.", "Deleting Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                list();
            }   
        }

        private void GW1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            comboBrand.SelectedItem = GW1.CurrentRow.Cells[12].Value.ToString();
            comboModel.SelectedItem = GW1.CurrentRow.Cells[13].Value.ToString();
            comboTransmission.SelectedItem = GW1.CurrentRow.Cells[14].Value.ToString();
            comboFuel.SelectedItem = GW1.CurrentRow.Cells[15].Value.ToString();
            comboColor.SelectedItem = GW1.CurrentRow.Cells[16].Value.ToString();
            lblDailyPrice.Text = GW1.CurrentRow.Cells[17].Value.ToString();
            dateTimeReceiving.Text = GW1.CurrentRow.Cells[18].Value.ToString();
            dateTimeDue.Text = GW1.CurrentRow.Cells[19].Value.ToString();
            lblDays.Text = GW1.CurrentRow.Cells[20].Value.ToString();
            lblTotalPrice.Text = GW1.CurrentRow.Cells[21].Value.ToString();
        }

        private void comboBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ADDING ARRAYLISTS
            ArrayList bmw = new ArrayList();
            ArrayList audi = new ArrayList();
            ArrayList mercedes = new ArrayList();
            ArrayList renault = new ArrayList();
            ArrayList volkswagen = new ArrayList();
            ArrayList skoda = new ArrayList();
            ArrayList seat = new ArrayList();
            ArrayList porsche = new ArrayList();
            ArrayList volvo = new ArrayList();
            ArrayList nissan = new ArrayList();
            ArrayList toyota = new ArrayList();

            //ADDING STRING ARRAY
            String[] bmw1 = new String[9] { "320Ci", "428i", "418i", "520", "525td", "M3", "Z4", "X5", "X6" };
            String[] audi1 = new String[9] { "A3", "A4", "A5", "A6", "A7", "A8", "Q7", "Q3", "Q5" };
            String[] mercedes1 = new String[7] { "CLA180", "A180","A200","E180","CLK500","Vito",
                "Sprinter"};
            String[] renault1 = new String[4] { "MEGANE", "CLIO", "FLUENCE", "TALISMAN" };
            String[] volkswagen1 = new String[9] { "GOLF", "JETTA", "POLO", "PASSAT", "CADDY",
                 "TIGUAN", "TOUAREG", "SCIROCCO", "AMAROK"};
            String[] skoda1 = new String[5] { "Fabia", "Superb", "Rapid", "Octavia", "Yeti" };
            String[] seat1 = new String[4] { "LEON", "IBIZA", "TOLEDO", "ATECA" };
            String[] porsche1 = new String[2] { "CAYENNE", "PANAMERA" };
            String[] volvo1 = new String[5] { "XC90", "V40", "S60", "V60", "S80" };
            String[] nissan1 = new String[5] { "XTRAIL", "QASHQAI", "JUKE", "NAVARA", "MICRA" };
            String[] toyota1 = new String[4] { "COROLLA", "AURIS", "AVENSIS", "YARIS" };

            SelectionSortModel(mercedes1.Length, mercedes1);
            SelectionSortModel(bmw1.Length, bmw1);
            SelectionSortModel(audi1.Length, audi1);
            SelectionSortModel(renault1.Length, renault1);
            SelectionSortModel(volkswagen1.Length, volkswagen1);
            SelectionSortModel(toyota1.Length, toyota1);
            SelectionSortModel(seat1.Length, seat1);
            SelectionSortModel(porsche1.Length, porsche1);
            SelectionSortModel(skoda1.Length, skoda1);
            SelectionSortModel(nissan1.Length, nissan1);
            SelectionSortModel(volvo1.Length, volvo1);

            //ADDING ARRAYS İN TO ARRAYLISTS
            bmw.AddRange(bmw1);
            audi.AddRange(audi1);
            mercedes.AddRange(mercedes1);
            renault.AddRange(renault1);
            volkswagen.AddRange(volkswagen1);
            skoda.AddRange(skoda1);
            seat.AddRange(seat1);
            porsche.AddRange(porsche1);
            volvo.AddRange(volvo1);
            nissan.AddRange(nissan1);
            toyota.AddRange(toyota1);

            //SELECTED ITEMS COMBOBOX ---> LİSTBOX
            if ("BMW".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in bmw)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("AUDI".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in audi)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("MERCEDES".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in mercedes)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("RENAULT".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in renault)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("VOLKSWAGEN".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in volkswagen)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("SKODA".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in skoda)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("SEAT".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in seat)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("PORSCHE".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in porsche)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("VOLVO".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in volvo)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("NISSAN".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in nissan)
                {
                    comboModel.Items.Add(car);
                }
            }

            else if ("TOYOTA".Equals(comboBrand.SelectedItem))
            {
                comboModel.Items.Clear();
                foreach (object car in toyota)
                {
                    comboModel.Items.Add(car);
                }
            }
        }

        ////////SELECTİON SORT FOR CARS' BRAND
        public string[] SelectionSortBrand()
        {
            string[] CarArray = new string[comboBrand.Items.Count];
            for (int i = 0; i < comboBrand.Items.Count; i++)
            {
                CarArray[i] = comboBrand.Items[i].ToString();
            }

            for (int i = 0; i < CarArray.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < CarArray.Length; j++)
                {
                    if (CarArray[j].CompareTo(CarArray[min]) < 0)
                    {
                        min = j;
                    }
                }

                string temp = CarArray[i];
                CarArray[i] = CarArray[min];
                CarArray[min] = temp;
                for (int v = 0; v < CarArray.Length; v++)
                {
                    comboBrand.Items[v] = CarArray[v];
                }
            }
            return CarArray;

        }
        //SELECTİON SORT FOR CARS' MODEL
        public string[] SelectionSortModel(int x, string[] modelArray)
        {
            string[] CarArray = new string[x];
            for (int i = 0; i < x; i++)
            {
                CarArray[i] = modelArray[i];
            }

            for (int i = 0; i < CarArray.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < CarArray.Length; j++)
                {
                    if (CarArray[j].CompareTo(CarArray[min]) < 0)
                    {
                        min = j;
                    }
                }
                string temp = CarArray[i];
                CarArray[i] = CarArray[min];
                CarArray[min] = temp;
                for (int v = 0; v < CarArray.Length; v++)
                {
                    modelArray[v] = CarArray[v];
                }
            }
            return CarArray;
        }
        //SELECTİON SORT FOR CARS' COLOUR
        public string[] SelectionSortColor()
        {
            string[] CarArray = new string[comboColor.Items.Count];
            for (int i = 0; i < comboColor.Items.Count; i++)
            {
                CarArray[i] = comboColor.Items[i].ToString();
            }

            for (int i = 0; i < CarArray.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < CarArray.Length; j++)
                {
                    if (CarArray[j].CompareTo(CarArray[min]) < 0)
                    {
                        min = j;
                    }
                }
                string temp = CarArray[i];
                CarArray[i] = CarArray[min];
                CarArray[min] = temp;
                for (int v = 0; v < CarArray.Length; v++)
                {
                    comboColor.Items[v] = CarArray[v];
                }
            }
            return CarArray;
        }

        private void dailyPrice_Click(object sender, EventArgs e)
        {
            if ("320Ci".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "220";
            }
            else if ("428i".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "280";
            }
            else if ("418i".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "250";
            }
            else if ("520".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "360";
            }
            else if ("525td".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "325";
            }
            else if ("M3".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "350";
            }
            else if ("Z4".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "500";
            }
            else if ("X5".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "540";
            }
            else if ("X6".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "580";
            }
            else if ("A3".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "180";
            }
            else if ("A4".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "240";
            }

            else if ("A5".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "350";
            }
            else if ("A6".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "400";
            }
            else if ("A7".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "450";
            }
            else if ("A8".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "500";
            }
            else if ("Q7".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "570";
            }
            else if ("Q5".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "350";
            }
            else if ("Q3".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "250";
            }
            else if ("CLA180".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "400";
            }
            else if ("A180".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "180";
            }
            else if ("A200".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "200";
            }
            else if ("E180".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "370";
            }
            else if ("CLK500".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "350";
            }
            else if ("Vito".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "500";
            }
            else if ("Sprinter".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "380";
            }
            else if ("MEGANE".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "180";
            }
            else if ("CLIO".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "100";
            }
            else if ("FLUENCE".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "130";
            }
            else if ("TALISMAN".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "280";
            }
            else if ("GOLF".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "180";
            }
            else if ("JETTA".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "160";
            }
            else if ("POLO".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "90";
            }
            else if ("PASSAT".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "260";
            }
            else if ("CADDY".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "120";
            }
            else if ("TIGUAN".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "300";
            }
            else if ("TOUAREG".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "500";
            }
            else if ("SCIROCCO".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "475";
            }
            else if ("AMAROK".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "325";
            }
            else if ("Fabia".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "90";
            }
            else if ("Superb".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "325";
            }
            else if ("Rapid".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "150";
            }
            else if ("Octavia".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "250";
            }
            else if ("Yeti".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "220";
            }
            else if ("LEON".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "180";
            }
            else if ("IBIZA".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "100";
            }
            else if ("TOLEDO".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "150";
            }
            else if ("ATECA".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "220";
            }
            else if ("CAYENNE".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "550";
            }
            else if ("PANAMERA".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "575";
            }
            else if ("XC90".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "600";
            }
            else if ("V40".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "200";
            }
            else if ("S60".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "275";
            }
            else if ("V60".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "250";
            }
            else if ("S80".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "300";
            }
            else if ("XTRAIL".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "280";
            }
            else if ("QASHQAI".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "220";
            }
            else if ("JUKE".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "125";
            }
            else if ("NAVARA".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "400";
            }
            else if ("MICRA".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "100";
            }
            else if ("COROLLA".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "200";
            }
            else if ("AURIS".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "180";
            }
            else if ("AVENSIS".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "150";
            }
            else if ("YARIS".Equals(comboModel.SelectedItem))
            {
                lblDailyPrice.Text = "";
                lblDailyPrice.Text = "90";
            }
        }

        private void btnTotalPrice_Click(object sender, EventArgs e)
        {
            int day;
            DateTime dtReceiving = new DateTime();
            DateTime dtDue = new DateTime();
            dtReceiving = dateTimeReceiving.Value;
            dtDue = dateTimeDue.Value;
            day = dayCalculation(dtDue, dtReceiving) + 1;
            lblTotalPrice.Visible = true;
            lblDays.Visible = true;
            lblDays.Text = day.ToString() + " days";

            if (day >= 90)
            {
                lblTotalPrice.Text = (day * Convert.ToInt32(lblDailyPrice.Text)) - (day * Convert.ToInt32(lblDailyPrice.Text) * 0.3) + " ₺";
            }
            else if (90 > day && day >= 60)
            {
                lblTotalPrice.Text = (day * Convert.ToInt32(lblDailyPrice.Text)) - (day * Convert.ToInt32(lblDailyPrice.Text) * 0.2) + " ₺";
            }
            else if (60 > day && day >= 30)
            {
                lblTotalPrice.Text = (day * Convert.ToInt32(lblDailyPrice.Text)) - (day * Convert.ToInt32(lblDailyPrice.Text) * 0.1) + " ₺";
            }
            else if (day < 30)
                lblTotalPrice.Text = (day * Convert.ToInt32(lblDailyPrice.Text)) + " ₺";
            else
                MessageBox.Show("Please choose dates!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public int dayCalculation(DateTime dtDue, DateTime dtReceiving)
        {
            TimeSpan time = new TimeSpan(); // zaman farkını bulmak adına kullanılacak olan nesne
            time = dtDue - dtReceiving;//metoda gelen 2 tarih arasındaki fark
            return Math.Abs(time.Days); // 2 tarih arasındaki farkın kaç gün olduğu döndürülüyor.
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (comboBrand.SelectedItem.ToString() != "" && comboModel.SelectedItem.ToString() != "" && comboTransmission.SelectedItem.ToString() != "" &&
                comboFuel.SelectedItem.ToString() != "" && comboColor.SelectedItem.ToString() != "" && lblDailyPrice.Text != "" &&
                dateTimeReceiving.Text != "" && dateTimeDue.Text != "" && lblDays.Text != "" && lblPrice.Text != "")
            {
                Connection.Open();
                Command.Connection = Connection;
                Command.CommandText = "UPDATE customer_list set brand='" + comboBrand.SelectedItem.ToString() + "',model='" + comboModel.SelectedItem.ToString() +
                "',transmission='" + comboTransmission.SelectedItem.ToString() + "',fuel='" + comboFuel.SelectedItem.ToString() +
                "',color='" + comboColor.SelectedItem.ToString() + "',price='" + lblDailyPrice.Text +
                "',receiving_date='" + dateTimeReceiving.Text + "',due_date='" + dateTimeDue.Text + "',days='" + lblDays.Text + "',total_price='" + lblPrice.Text +
                "' WHERE model='" + comboModel.SelectedItem.ToString() + "'";
                Command.ExecuteNonQuery();
                Connection.Close();
                MessageBox.Show("Registration Successful.", "Registration Completed", MessageBoxButtons.OK, MessageBoxIcon.Information); Connection.Close();
                comboBrand.SelectedIndex = -1;
                comboModel.SelectedIndex = -1;
                comboTransmission.SelectedIndex = -1;
                comboFuel.SelectedIndex = -1;
                comboColor.SelectedIndex = -1;
                lblDailyPrice.Text = "";
                dateTimeReceiving.Text = "";
                dateTimeDue.Text = "";
                lblDays.Text = "";
                lblPrice.Text = "";
                list();
            }
            else
            {
                MessageBox.Show("Please fill in the blanks!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
