﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rent_A_Car
{
    public partial class CarList : Form
    {
        OleDbConnection Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
        OleDbCommand Command = new OleDbCommand();
        OleDbDataAdapter adtr = new OleDbDataAdapter();
        DataSet ds = new System.Data.DataSet();
        public CarList()
        {
            InitializeComponent();
        }

        void list()
        {
            Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
            adtr = new OleDbDataAdapter("Select *from car_list", Connection);
            ds = new DataSet();
            Connection.Open();
            adtr.Fill(ds, "car_list");
            dataGridView1.DataSource = ds.Tables["car_list"];
            Connection.Close();
        }

        private void CarList_Load(object sender, EventArgs e)
        {
            list();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtnumberplate.Text != "" && txtbrand.Text != "" && txtmodel.Text != "" &&  txtyear.Text != "" &&
                cmbxtransmission.SelectedItem.ToString() != "" && cmbxfuel.SelectedItem.ToString() != "" && cmbxcolor.Text != "" &&
                txtprice.Text != "")
            {
                Connection.Open();
                Command.Connection = Connection;
                Command.CommandText = "Insert Into car_list(car_numberplate,car_brand,car_model,car_year,transmission,fuel,color,price) Values ('" +
                txtnumberplate.Text + "','" + txtbrand.Text + "','" + txtmodel.Text + "','" + txtyear.Text + "','" +
                cmbxtransmission.SelectedItem.ToString() + "','" + cmbxfuel.SelectedItem.ToString() + "','" + cmbxcolor.Text + "','" +
                txtprice.Text + "')";
                Command.ExecuteNonQuery();
                Command.Dispose();
                Connection.Close();
                MessageBox.Show("Registration Successful.", "Registration Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Connection.Close();              
                list();
                txtnumberplate.Text = "";
                txtbrand.Text = "";
                txtmodel.Text = "";
                txtyear.Text = "";
                cmbxtransmission.SelectedIndex = -1;
                cmbxfuel.SelectedIndex = -1;
                cmbxcolor.SelectedIndex = -1;
                txtprice.Text = "";
            }
            else
            {
                MessageBox.Show("Please fill in the blanks!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtnumberplategb2.Text != "" && txtbrandgb2.Text != "" && txtmodelgb2.Text != "" && txtyeargb2.Text != "" &&
                cmbxtransmissiongb2.SelectedItem.ToString() != "" && cmbxfuelgb2.SelectedItem.ToString() != "" && cmbxcolorgb2.Text != "" &&
                txtpricegb2.Text != "")
            {
                Connection.Open();
                Command.Connection = Connection;
                Command.CommandText = "UPDATE car_list set car_numberplate='" + txtnumberplategb2.Text + "',car_brand='" + txtbrandgb2.Text +
                "',car_model='" + txtmodelgb2.Text + "',car_year='" + txtyeargb2.Text +
                "',transmission='" + cmbxtransmissiongb2.SelectedItem.ToString() + "',fuel='" + cmbxfuelgb2.SelectedItem.ToString() +
                "',color='" + cmbxcolorgb2.SelectedItem.ToString() + "',price='" + txtpricegb2.Text + "' WHERE car_numberplate='" + txtnumberplategb2.Text + "'";
                Command.ExecuteNonQuery();
                Connection.Close();
                MessageBox.Show("Registration Successful.", "Registration Completed", MessageBoxButtons.OK, MessageBoxIcon.Information); Connection.Close();
                txtnumberplategb2.Text = "";
                txtbrandgb2.Text = "";
                txtmodelgb2.Text = "";
                txtyeargb2.Text = "";
                cmbxtransmissiongb2.SelectedIndex = -1;
                cmbxfuelgb2.SelectedIndex = -1;
                cmbxcolorgb2.SelectedIndex = -1;
                txtpricegb2.Text = "";
                list();
            }
            else
            {
                MessageBox.Show("Please fill in the blanks!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            txtnumberplategb2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            txtbrandgb2.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            txtmodelgb2.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            txtyeargb2.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            cmbxtransmissiongb2.SelectedItem = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            cmbxfuelgb2.SelectedItem = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            cmbxcolorgb2.SelectedItem = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            txtpricegb2.Text = dataGridView1.CurrentRow.Cells[8].Value.ToString();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbAddCar.Visible = true;
            gbUpdateCar.Visible = false;
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbUpdateCar.Visible = true;
            gbAddCar.Visible = false;
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure that you want to delete ?", "Warning", MessageBoxButtons.YesNo,
              MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Connection.Open();
                Command.Connection = Connection;
                Command.CommandText = "DELETE FROM car_list WHERE car_numberplate ='" + dataGridView1.CurrentRow.Cells[1].Value.ToString() + "'";
                Command.ExecuteNonQuery();
                Connection.Close();
                MessageBox.Show("Deleting Successful.", "Deleting Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                list();
            }   
        }

        private void txtsearch_TextChanged(object sender, EventArgs e)
        {
            Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
            adtr = new OleDbDataAdapter("Select *from car_list where car_brand like '" + txtsearch.Text + "%'", Connection);
            ds = new DataSet();
            Connection.Open();
            adtr.Fill(ds, "car_list");
            dataGridView1.DataSource = ds.Tables["car_list"];
            Connection.Close();
        }

        private void txtyeargb2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtpricegb2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtbrandgb2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
                && !char.IsSeparator(e.KeyChar);
        }

        private void txtprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtyear_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtbrand_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
               && !char.IsSeparator(e.KeyChar);
        }

    }
}
