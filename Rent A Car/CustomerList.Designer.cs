﻿namespace Rent_A_Car
{
    partial class CustomerList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerList));
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.GW1 = new System.Windows.Forms.DataGridView();
            this.btndelete = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.grpbxcar = new System.Windows.Forms.GroupBox();
            this.comboColor = new System.Windows.Forms.ComboBox();
            this.comboFuel = new System.Windows.Forms.ComboBox();
            this.comboTransmission = new System.Windows.Forms.ComboBox();
            this.comboModel = new System.Windows.Forms.ComboBox();
            this.comboBrand = new System.Windows.Forms.ComboBox();
            this.dailyPrice = new System.Windows.Forms.Button();
            this.lblDailyPrice = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblColor = new System.Windows.Forms.Label();
            this.lblFuel = new System.Windows.Forms.Label();
            this.lblTransmission = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblBrand = new System.Windows.Forms.Label();
            this.grpbxrenting = new System.Windows.Forms.GroupBox();
            this.btnTotalPrice = new System.Windows.Forms.Button();
            this.lblDays = new System.Windows.Forms.Label();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.lblReceiving = new System.Windows.Forms.Label();
            this.dateTimeDue = new System.Windows.Forms.DateTimePicker();
            this.dateTimeReceiving = new System.Windows.Forms.DateTimePicker();
            this.lblDue = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.GW1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grpbxcar.SuspendLayout();
            this.grpbxrenting.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtSearch.Location = new System.Drawing.Point(76, 29);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(2);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(141, 24);
            this.txtSearch.TabIndex = 4;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // GW1
            // 
            this.GW1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GW1.Location = new System.Drawing.Point(11, 62);
            this.GW1.Margin = new System.Windows.Forms.Padding(2);
            this.GW1.Name = "GW1";
            this.GW1.RowTemplate.Height = 24;
            this.GW1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GW1.Size = new System.Drawing.Size(598, 216);
            this.GW1.TabIndex = 5;
            this.GW1.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.GW1_CellEnter);
            // 
            // btndelete
            // 
            this.btndelete.BackColor = System.Drawing.Color.Transparent;
            this.btndelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btndelete.FlatAppearance.BorderSize = 0;
            this.btndelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btndelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btndelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btndelete.Image = ((System.Drawing.Image)(resources.GetObject("btndelete.Image")));
            this.btndelete.Location = new System.Drawing.Point(375, 12);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(45, 45);
            this.btndelete.TabIndex = 6;
            this.btndelete.UseVisualStyleBackColor = false;
            this.btndelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(13, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 52);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // grpbxcar
            // 
            this.grpbxcar.Controls.Add(this.comboColor);
            this.grpbxcar.Controls.Add(this.comboFuel);
            this.grpbxcar.Controls.Add(this.comboTransmission);
            this.grpbxcar.Controls.Add(this.comboModel);
            this.grpbxcar.Controls.Add(this.comboBrand);
            this.grpbxcar.Controls.Add(this.dailyPrice);
            this.grpbxcar.Controls.Add(this.lblDailyPrice);
            this.grpbxcar.Controls.Add(this.lblPrice);
            this.grpbxcar.Controls.Add(this.lblColor);
            this.grpbxcar.Controls.Add(this.lblFuel);
            this.grpbxcar.Controls.Add(this.lblTransmission);
            this.grpbxcar.Controls.Add(this.lblModel);
            this.grpbxcar.Controls.Add(this.lblBrand);
            this.grpbxcar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.grpbxcar.Location = new System.Drawing.Point(13, 283);
            this.grpbxcar.Name = "grpbxcar";
            this.grpbxcar.Size = new System.Drawing.Size(278, 298);
            this.grpbxcar.TabIndex = 11;
            this.grpbxcar.TabStop = false;
            this.grpbxcar.Text = "Car Information";
            // 
            // comboColor
            // 
            this.comboColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboColor.FormattingEnabled = true;
            this.comboColor.Items.AddRange(new object[] {
            "White",
            "Black",
            "Metalic Grey",
            "Blue",
            "Red",
            "Yellow",
            "Orange",
            "Green"});
            this.comboColor.Location = new System.Drawing.Point(130, 171);
            this.comboColor.Name = "comboColor";
            this.comboColor.Size = new System.Drawing.Size(142, 28);
            this.comboColor.TabIndex = 59;
            // 
            // comboFuel
            // 
            this.comboFuel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboFuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboFuel.FormattingEnabled = true;
            this.comboFuel.Items.AddRange(new object[] {
            "Diesel",
            "Gasoline"});
            this.comboFuel.Location = new System.Drawing.Point(130, 133);
            this.comboFuel.Name = "comboFuel";
            this.comboFuel.Size = new System.Drawing.Size(142, 28);
            this.comboFuel.TabIndex = 58;
            // 
            // comboTransmission
            // 
            this.comboTransmission.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTransmission.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboTransmission.FormattingEnabled = true;
            this.comboTransmission.Items.AddRange(new object[] {
            "Automatic",
            "Manuel"});
            this.comboTransmission.Location = new System.Drawing.Point(130, 99);
            this.comboTransmission.Name = "comboTransmission";
            this.comboTransmission.Size = new System.Drawing.Size(142, 28);
            this.comboTransmission.TabIndex = 57;
            // 
            // comboModel
            // 
            this.comboModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboModel.FormattingEnabled = true;
            this.comboModel.Location = new System.Drawing.Point(130, 63);
            this.comboModel.Name = "comboModel";
            this.comboModel.Size = new System.Drawing.Size(142, 28);
            this.comboModel.TabIndex = 56;
            // 
            // comboBrand
            // 
            this.comboBrand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboBrand.FormattingEnabled = true;
            this.comboBrand.Items.AddRange(new object[] {
            "BMW",
            "AUDI",
            "MERCEDES",
            "RENAULT",
            "VOLKSWAGEN",
            "SKODA",
            "SEAT",
            "PORSCHE",
            "VOLVO",
            "NISSAN",
            "TOYOTA"});
            this.comboBrand.Location = new System.Drawing.Point(130, 27);
            this.comboBrand.Name = "comboBrand";
            this.comboBrand.Size = new System.Drawing.Size(142, 28);
            this.comboBrand.TabIndex = 55;
            this.comboBrand.SelectedIndexChanged += new System.EventHandler(this.comboBrand_SelectedIndexChanged);
            // 
            // dailyPrice
            // 
            this.dailyPrice.BackColor = System.Drawing.Color.Transparent;
            this.dailyPrice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dailyPrice.FlatAppearance.BorderSize = 0;
            this.dailyPrice.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.dailyPrice.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.dailyPrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dailyPrice.ForeColor = System.Drawing.Color.Transparent;
            this.dailyPrice.Image = ((System.Drawing.Image)(resources.GetObject("dailyPrice.Image")));
            this.dailyPrice.Location = new System.Drawing.Point(191, 231);
            this.dailyPrice.Margin = new System.Windows.Forms.Padding(2);
            this.dailyPrice.Name = "dailyPrice";
            this.dailyPrice.Size = new System.Drawing.Size(60, 67);
            this.dailyPrice.TabIndex = 54;
            this.dailyPrice.UseVisualStyleBackColor = false;
            this.dailyPrice.Click += new System.EventHandler(this.dailyPrice_Click);
            // 
            // lblDailyPrice
            // 
            this.lblDailyPrice.AutoSize = true;
            this.lblDailyPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblDailyPrice.Location = new System.Drawing.Point(117, 205);
            this.lblDailyPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDailyPrice.Name = "lblDailyPrice";
            this.lblDailyPrice.Size = new System.Drawing.Size(0, 25);
            this.lblDailyPrice.TabIndex = 53;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPrice.Location = new System.Drawing.Point(5, 210);
            this.lblPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(48, 20);
            this.lblPrice.TabIndex = 52;
            this.lblPrice.Text = "Price:";
            // 
            // lblColor
            // 
            this.lblColor.AutoSize = true;
            this.lblColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblColor.Location = new System.Drawing.Point(5, 174);
            this.lblColor.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(50, 20);
            this.lblColor.TabIndex = 43;
            this.lblColor.Text = "Color:";
            // 
            // lblFuel
            // 
            this.lblFuel.AutoSize = true;
            this.lblFuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFuel.Location = new System.Drawing.Point(5, 138);
            this.lblFuel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFuel.Name = "lblFuel";
            this.lblFuel.Size = new System.Drawing.Size(44, 20);
            this.lblFuel.TabIndex = 42;
            this.lblFuel.Text = "Fuel:";
            // 
            // lblTransmission
            // 
            this.lblTransmission.AutoSize = true;
            this.lblTransmission.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTransmission.Location = new System.Drawing.Point(5, 102);
            this.lblTransmission.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTransmission.Name = "lblTransmission";
            this.lblTransmission.Size = new System.Drawing.Size(106, 20);
            this.lblTransmission.TabIndex = 41;
            this.lblTransmission.Text = "Transmission:";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblModel.Location = new System.Drawing.Point(5, 66);
            this.lblModel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(56, 20);
            this.lblModel.TabIndex = 40;
            this.lblModel.Text = "Model:";
            // 
            // lblBrand
            // 
            this.lblBrand.AutoSize = true;
            this.lblBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblBrand.Location = new System.Drawing.Point(5, 30);
            this.lblBrand.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBrand.Name = "lblBrand";
            this.lblBrand.Size = new System.Drawing.Size(56, 20);
            this.lblBrand.TabIndex = 38;
            this.lblBrand.Text = "Brand:";
            // 
            // grpbxrenting
            // 
            this.grpbxrenting.Controls.Add(this.btnTotalPrice);
            this.grpbxrenting.Controls.Add(this.lblDays);
            this.grpbxrenting.Controls.Add(this.lblTotalPrice);
            this.grpbxrenting.Controls.Add(this.lblReceiving);
            this.grpbxrenting.Controls.Add(this.dateTimeDue);
            this.grpbxrenting.Controls.Add(this.dateTimeReceiving);
            this.grpbxrenting.Controls.Add(this.lblDue);
            this.grpbxrenting.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.grpbxrenting.Location = new System.Drawing.Point(297, 283);
            this.grpbxrenting.Name = "grpbxrenting";
            this.grpbxrenting.Size = new System.Drawing.Size(312, 230);
            this.grpbxrenting.TabIndex = 55;
            this.grpbxrenting.TabStop = false;
            this.grpbxrenting.Text = "Renting Information";
            // 
            // btnTotalPrice
            // 
            this.btnTotalPrice.BackColor = System.Drawing.Color.Transparent;
            this.btnTotalPrice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTotalPrice.FlatAppearance.BorderSize = 0;
            this.btnTotalPrice.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnTotalPrice.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnTotalPrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTotalPrice.ForeColor = System.Drawing.Color.Transparent;
            this.btnTotalPrice.Image = ((System.Drawing.Image)(resources.GetObject("btnTotalPrice.Image")));
            this.btnTotalPrice.Location = new System.Drawing.Point(201, 133);
            this.btnTotalPrice.Margin = new System.Windows.Forms.Padding(2);
            this.btnTotalPrice.Name = "btnTotalPrice";
            this.btnTotalPrice.Size = new System.Drawing.Size(90, 90);
            this.btnTotalPrice.TabIndex = 51;
            this.btnTotalPrice.UseVisualStyleBackColor = false;
            this.btnTotalPrice.Click += new System.EventHandler(this.btnTotalPrice_Click);
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblDays.Location = new System.Drawing.Point(46, 122);
            this.lblDays.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(62, 25);
            this.lblDays.TabIndex = 50;
            this.lblDays.Text = "days";
            this.lblDays.Visible = false;
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.AutoSize = true;
            this.lblTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTotalPrice.Location = new System.Drawing.Point(46, 169);
            this.lblTotalPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(64, 25);
            this.lblTotalPrice.TabIndex = 49;
            this.lblTotalPrice.Text = "price";
            this.lblTotalPrice.Visible = false;
            // 
            // lblReceiving
            // 
            this.lblReceiving.AutoSize = true;
            this.lblReceiving.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblReceiving.Location = new System.Drawing.Point(5, 37);
            this.lblReceiving.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReceiving.Name = "lblReceiving";
            this.lblReceiving.Size = new System.Drawing.Size(121, 20);
            this.lblReceiving.TabIndex = 46;
            this.lblReceiving.Text = "Receiving Date:";
            // 
            // dateTimeDue
            // 
            this.dateTimeDue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dateTimeDue.Location = new System.Drawing.Point(134, 72);
            this.dateTimeDue.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimeDue.Name = "dateTimeDue";
            this.dateTimeDue.Size = new System.Drawing.Size(173, 26);
            this.dateTimeDue.TabIndex = 48;
            // 
            // dateTimeReceiving
            // 
            this.dateTimeReceiving.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dateTimeReceiving.Location = new System.Drawing.Point(134, 32);
            this.dateTimeReceiving.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimeReceiving.Name = "dateTimeReceiving";
            this.dateTimeReceiving.Size = new System.Drawing.Size(173, 26);
            this.dateTimeReceiving.TabIndex = 45;
            // 
            // lblDue
            // 
            this.lblDue.AutoSize = true;
            this.lblDue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblDue.Location = new System.Drawing.Point(5, 77);
            this.lblDue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDue.Name = "lblDue";
            this.lblDue.Size = new System.Drawing.Size(82, 20);
            this.lblDue.TabIndex = 47;
            this.lblDue.Text = "Due Date:";
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.FlatAppearance.BorderSize = 0;
            this.btnUpdate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnUpdate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.ForeColor = System.Drawing.Color.Transparent;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(484, 521);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(60, 60);
            this.btnUpdate.TabIndex = 56;
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // CustomerList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 593);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.grpbxrenting);
            this.Controls.Add(this.grpbxcar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.GW1);
            this.Controls.Add(this.txtSearch);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CustomerList";
            this.Text = "Customer List";
            this.Load += new System.EventHandler(this.CustomerList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GW1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grpbxcar.ResumeLayout(false);
            this.grpbxcar.PerformLayout();
            this.grpbxrenting.ResumeLayout(false);
            this.grpbxrenting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSearch;
        public System.Windows.Forms.DataGridView GW1;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox grpbxcar;
        private System.Windows.Forms.Label lblDailyPrice;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.Label lblFuel;
        private System.Windows.Forms.Label lblTransmission;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblBrand;
        private System.Windows.Forms.GroupBox grpbxrenting;
        private System.Windows.Forms.Label lblDays;
        private System.Windows.Forms.Label lblTotalPrice;
        private System.Windows.Forms.Label lblReceiving;
        private System.Windows.Forms.DateTimePicker dateTimeDue;
        private System.Windows.Forms.DateTimePicker dateTimeReceiving;
        private System.Windows.Forms.Label lblDue;
        private System.Windows.Forms.Button dailyPrice;
        private System.Windows.Forms.Button btnTotalPrice;
        private System.Windows.Forms.ComboBox comboColor;
        private System.Windows.Forms.ComboBox comboFuel;
        private System.Windows.Forms.ComboBox comboTransmission;
        private System.Windows.Forms.ComboBox comboModel;
        private System.Windows.Forms.ComboBox comboBrand;
        private System.Windows.Forms.Button btnUpdate;
    }
}