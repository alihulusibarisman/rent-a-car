﻿namespace Rent_A_Car
{
    partial class CarList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CarList));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.gbAddCar = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.cmbxcolor = new System.Windows.Forms.ComboBox();
            this.cmbxtransmission = new System.Windows.Forms.ComboBox();
            this.cmbxfuel = new System.Windows.Forms.ComboBox();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.txtyear = new System.Windows.Forms.TextBox();
            this.txtmodel = new System.Windows.Forms.TextBox();
            this.txtbrand = new System.Windows.Forms.TextBox();
            this.txtnumberplate = new System.Windows.Forms.TextBox();
            this.txtsearch = new System.Windows.Forms.TextBox();
            this.gbUpdateCar = new System.Windows.Forms.GroupBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cmbxcolorgb2 = new System.Windows.Forms.ComboBox();
            this.cmbxtransmissiongb2 = new System.Windows.Forms.ComboBox();
            this.cmbxfuelgb2 = new System.Windows.Forms.ComboBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtpricegb2 = new System.Windows.Forms.TextBox();
            this.txtyeargb2 = new System.Windows.Forms.TextBox();
            this.txtmodelgb2 = new System.Windows.Forms.TextBox();
            this.txtbrandgb2 = new System.Windows.Forms.TextBox();
            this.txtnumberplategb2 = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.gbAddCar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.gbUpdateCar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.updateToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(833, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.addToolStripMenuItem.Text = "ADD";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            this.updateToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.updateToolStripMenuItem.Text = "UPDATE";
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.deleteToolStripMenuItem.Text = "DELETE";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(11, 60);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(804, 231);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEnter);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEnter);
            // 
            // gbAddCar
            // 
            this.gbAddCar.Controls.Add(this.btnAdd);
            this.gbAddCar.Controls.Add(this.pictureBox9);
            this.gbAddCar.Controls.Add(this.pictureBox10);
            this.gbAddCar.Controls.Add(this.pictureBox11);
            this.gbAddCar.Controls.Add(this.pictureBox12);
            this.gbAddCar.Controls.Add(this.pictureBox13);
            this.gbAddCar.Controls.Add(this.pictureBox14);
            this.gbAddCar.Controls.Add(this.pictureBox15);
            this.gbAddCar.Controls.Add(this.pictureBox16);
            this.gbAddCar.Controls.Add(this.cmbxcolor);
            this.gbAddCar.Controls.Add(this.cmbxtransmission);
            this.gbAddCar.Controls.Add(this.cmbxfuel);
            this.gbAddCar.Controls.Add(this.txtprice);
            this.gbAddCar.Controls.Add(this.txtyear);
            this.gbAddCar.Controls.Add(this.txtmodel);
            this.gbAddCar.Controls.Add(this.txtbrand);
            this.gbAddCar.Controls.Add(this.txtnumberplate);
            this.gbAddCar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbAddCar.Location = new System.Drawing.Point(11, 294);
            this.gbAddCar.Margin = new System.Windows.Forms.Padding(2);
            this.gbAddCar.Name = "gbAddCar";
            this.gbAddCar.Padding = new System.Windows.Forms.Padding(2);
            this.gbAddCar.Size = new System.Drawing.Size(804, 185);
            this.gbAddCar.TabIndex = 5;
            this.gbAddCar.TabStop = false;
            this.gbAddCar.Text = "Add Car";
            this.gbAddCar.Visible = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAdd.ForeColor = System.Drawing.Color.Transparent;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(388, 127);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(50, 50);
            this.btnAdd.TabIndex = 16;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(205, 20);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(132, 64);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 31;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(110, 20);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(82, 62);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 25;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(6, 33);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(95, 46);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 30;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(353, 23);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(66, 60);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 26;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(634, 21);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(62, 59);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 27;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(719, 19);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(70, 63);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 28;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(447, 23);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(57, 60);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 29;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox16.Image")));
            this.pictureBox16.Location = new System.Drawing.Point(545, 21);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(61, 61);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 24;
            this.pictureBox16.TabStop = false;
            // 
            // cmbxcolor
            // 
            this.cmbxcolor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxcolor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxcolor.FormattingEnabled = true;
            this.cmbxcolor.Items.AddRange(new object[] {
            "White",
            "Black",
            "Metalic Grey",
            "Blue",
            "Red",
            "Yellow",
            "Orange",
            "Green"});
            this.cmbxcolor.Location = new System.Drawing.Point(621, 91);
            this.cmbxcolor.Name = "cmbxcolor";
            this.cmbxcolor.Size = new System.Drawing.Size(91, 26);
            this.cmbxcolor.TabIndex = 7;
            // 
            // cmbxtransmission
            // 
            this.cmbxtransmission.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxtransmission.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxtransmission.FormattingEnabled = true;
            this.cmbxtransmission.Items.AddRange(new object[] {
            "Automatic",
            "Manuel"});
            this.cmbxtransmission.Location = new System.Drawing.Point(432, 91);
            this.cmbxtransmission.Name = "cmbxtransmission";
            this.cmbxtransmission.Size = new System.Drawing.Size(95, 26);
            this.cmbxtransmission.TabIndex = 5;
            // 
            // cmbxfuel
            // 
            this.cmbxfuel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxfuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxfuel.FormattingEnabled = true;
            this.cmbxfuel.Items.AddRange(new object[] {
            "Diesel",
            "Gasoline"});
            this.cmbxfuel.Location = new System.Drawing.Point(533, 91);
            this.cmbxfuel.Name = "cmbxfuel";
            this.cmbxfuel.Size = new System.Drawing.Size(82, 26);
            this.cmbxfuel.TabIndex = 6;
            // 
            // txtprice
            // 
            this.txtprice.Location = new System.Drawing.Point(719, 91);
            this.txtprice.Margin = new System.Windows.Forms.Padding(2);
            this.txtprice.MaxLength = 4;
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(73, 26);
            this.txtprice.TabIndex = 8;
            this.txtprice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprice_KeyPress);
            // 
            // txtyear
            // 
            this.txtyear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtyear.Location = new System.Drawing.Point(353, 91);
            this.txtyear.Margin = new System.Windows.Forms.Padding(2);
            this.txtyear.MaxLength = 4;
            this.txtyear.Name = "txtyear";
            this.txtyear.Size = new System.Drawing.Size(66, 26);
            this.txtyear.TabIndex = 4;
            this.txtyear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtyear_KeyPress);
            // 
            // txtmodel
            // 
            this.txtmodel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtmodel.Location = new System.Drawing.Point(214, 91);
            this.txtmodel.Margin = new System.Windows.Forms.Padding(2);
            this.txtmodel.MaxLength = 15;
            this.txtmodel.Name = "txtmodel";
            this.txtmodel.Size = new System.Drawing.Size(115, 26);
            this.txtmodel.TabIndex = 3;
            // 
            // txtbrand
            // 
            this.txtbrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtbrand.Location = new System.Drawing.Point(115, 91);
            this.txtbrand.Margin = new System.Windows.Forms.Padding(2);
            this.txtbrand.MaxLength = 15;
            this.txtbrand.Name = "txtbrand";
            this.txtbrand.Size = new System.Drawing.Size(76, 26);
            this.txtbrand.TabIndex = 2;
            this.txtbrand.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbrand_KeyPress);
            // 
            // txtnumberplate
            // 
            this.txtnumberplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtnumberplate.Location = new System.Drawing.Point(4, 91);
            this.txtnumberplate.Margin = new System.Windows.Forms.Padding(2);
            this.txtnumberplate.MaxLength = 9;
            this.txtnumberplate.Name = "txtnumberplate";
            this.txtnumberplate.Size = new System.Drawing.Size(100, 26);
            this.txtnumberplate.TabIndex = 1;
            // 
            // txtsearch
            // 
            this.txtsearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtsearch.Location = new System.Drawing.Point(11, 29);
            this.txtsearch.Name = "txtsearch";
            this.txtsearch.Size = new System.Drawing.Size(159, 26);
            this.txtsearch.TabIndex = 6;
            this.txtsearch.TextChanged += new System.EventHandler(this.txtsearch_TextChanged);
            // 
            // gbUpdateCar
            // 
            this.gbUpdateCar.Controls.Add(this.pictureBox8);
            this.gbUpdateCar.Controls.Add(this.pictureBox7);
            this.gbUpdateCar.Controls.Add(this.pictureBox6);
            this.gbUpdateCar.Controls.Add(this.pictureBox5);
            this.gbUpdateCar.Controls.Add(this.pictureBox4);
            this.gbUpdateCar.Controls.Add(this.pictureBox3);
            this.gbUpdateCar.Controls.Add(this.pictureBox2);
            this.gbUpdateCar.Controls.Add(this.pictureBox1);
            this.gbUpdateCar.Controls.Add(this.cmbxcolorgb2);
            this.gbUpdateCar.Controls.Add(this.cmbxtransmissiongb2);
            this.gbUpdateCar.Controls.Add(this.cmbxfuelgb2);
            this.gbUpdateCar.Controls.Add(this.btnUpdate);
            this.gbUpdateCar.Controls.Add(this.txtpricegb2);
            this.gbUpdateCar.Controls.Add(this.txtyeargb2);
            this.gbUpdateCar.Controls.Add(this.txtmodelgb2);
            this.gbUpdateCar.Controls.Add(this.txtbrandgb2);
            this.gbUpdateCar.Controls.Add(this.txtnumberplategb2);
            this.gbUpdateCar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbUpdateCar.Location = new System.Drawing.Point(11, 292);
            this.gbUpdateCar.Margin = new System.Windows.Forms.Padding(2);
            this.gbUpdateCar.Name = "gbUpdateCar";
            this.gbUpdateCar.Padding = new System.Windows.Forms.Padding(2);
            this.gbUpdateCar.Size = new System.Drawing.Size(804, 187);
            this.gbUpdateCar.TabIndex = 20;
            this.gbUpdateCar.TabStop = false;
            this.gbUpdateCar.Text = "Update Car";
            this.gbUpdateCar.Visible = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(205, 26);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(132, 64);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 23;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(110, 26);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(82, 62);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 21;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(6, 39);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(95, 46);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 22;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(353, 29);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(66, 60);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 21;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(634, 27);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(62, 59);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 21;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(719, 25);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(70, 63);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 21;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(447, 29);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(57, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(545, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(61, 61);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // cmbxcolorgb2
            // 
            this.cmbxcolorgb2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxcolorgb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxcolorgb2.FormattingEnabled = true;
            this.cmbxcolorgb2.Items.AddRange(new object[] {
            "White",
            "Black",
            "Metalic Grey",
            "Blue",
            "Red",
            "Yellow",
            "Orange",
            "Green"});
            this.cmbxcolorgb2.Location = new System.Drawing.Point(621, 93);
            this.cmbxcolorgb2.Name = "cmbxcolorgb2";
            this.cmbxcolorgb2.Size = new System.Drawing.Size(91, 28);
            this.cmbxcolorgb2.TabIndex = 7;
            // 
            // cmbxtransmissiongb2
            // 
            this.cmbxtransmissiongb2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxtransmissiongb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxtransmissiongb2.FormattingEnabled = true;
            this.cmbxtransmissiongb2.Items.AddRange(new object[] {
            "Automatic",
            "Manuel"});
            this.cmbxtransmissiongb2.Location = new System.Drawing.Point(432, 93);
            this.cmbxtransmissiongb2.Name = "cmbxtransmissiongb2";
            this.cmbxtransmissiongb2.Size = new System.Drawing.Size(95, 28);
            this.cmbxtransmissiongb2.TabIndex = 5;
            // 
            // cmbxfuelgb2
            // 
            this.cmbxfuelgb2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxfuelgb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbxfuelgb2.FormattingEnabled = true;
            this.cmbxfuelgb2.Items.AddRange(new object[] {
            "Diesel",
            "Gasoline"});
            this.cmbxfuelgb2.Location = new System.Drawing.Point(533, 93);
            this.cmbxfuelgb2.Name = "cmbxfuelgb2";
            this.cmbxfuelgb2.Size = new System.Drawing.Size(82, 28);
            this.cmbxfuelgb2.TabIndex = 6;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.FlatAppearance.BorderSize = 0;
            this.btnUpdate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnUpdate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnUpdate.ForeColor = System.Drawing.Color.Transparent;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(388, 127);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(50, 50);
            this.btnUpdate.TabIndex = 16;
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtpricegb2
            // 
            this.txtpricegb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtpricegb2.Location = new System.Drawing.Point(718, 93);
            this.txtpricegb2.Margin = new System.Windows.Forms.Padding(2);
            this.txtpricegb2.MaxLength = 4;
            this.txtpricegb2.Name = "txtpricegb2";
            this.txtpricegb2.Size = new System.Drawing.Size(74, 27);
            this.txtpricegb2.TabIndex = 8;
            this.txtpricegb2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpricegb2_KeyPress);
            // 
            // txtyeargb2
            // 
            this.txtyeargb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtyeargb2.Location = new System.Drawing.Point(353, 94);
            this.txtyeargb2.Margin = new System.Windows.Forms.Padding(2);
            this.txtyeargb2.MaxLength = 4;
            this.txtyeargb2.Name = "txtyeargb2";
            this.txtyeargb2.Size = new System.Drawing.Size(66, 27);
            this.txtyeargb2.TabIndex = 4;
            this.txtyeargb2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtyeargb2_KeyPress);
            // 
            // txtmodelgb2
            // 
            this.txtmodelgb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtmodelgb2.Location = new System.Drawing.Point(214, 94);
            this.txtmodelgb2.Margin = new System.Windows.Forms.Padding(2);
            this.txtmodelgb2.MaxLength = 15;
            this.txtmodelgb2.Name = "txtmodelgb2";
            this.txtmodelgb2.Size = new System.Drawing.Size(115, 27);
            this.txtmodelgb2.TabIndex = 3;
            // 
            // txtbrandgb2
            // 
            this.txtbrandgb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtbrandgb2.Location = new System.Drawing.Point(115, 94);
            this.txtbrandgb2.Margin = new System.Windows.Forms.Padding(2);
            this.txtbrandgb2.MaxLength = 15;
            this.txtbrandgb2.Name = "txtbrandgb2";
            this.txtbrandgb2.Size = new System.Drawing.Size(76, 27);
            this.txtbrandgb2.TabIndex = 2;
            this.txtbrandgb2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbrandgb2_KeyPress);
            // 
            // txtnumberplategb2
            // 
            this.txtnumberplategb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtnumberplategb2.Location = new System.Drawing.Point(4, 94);
            this.txtnumberplategb2.Margin = new System.Windows.Forms.Padding(2);
            this.txtnumberplategb2.MaxLength = 9;
            this.txtnumberplategb2.Name = "txtnumberplategb2";
            this.txtnumberplategb2.Size = new System.Drawing.Size(100, 27);
            this.txtnumberplategb2.TabIndex = 1;
            // 
            // CarList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 490);
            this.Controls.Add(this.gbUpdateCar);
            this.Controls.Add(this.txtsearch);
            this.Controls.Add(this.gbAddCar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CarList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Car List";
            this.Load += new System.EventHandler(this.CarList_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.gbAddCar.ResumeLayout(false);
            this.gbAddCar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.gbUpdateCar.ResumeLayout(false);
            this.gbUpdateCar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox gbAddCar;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.TextBox txtyear;
        private System.Windows.Forms.TextBox txtmodel;
        private System.Windows.Forms.TextBox txtbrand;
        private System.Windows.Forms.TextBox txtnumberplate;
        private System.Windows.Forms.ComboBox cmbxtransmission;
        private System.Windows.Forms.ComboBox cmbxfuel;
        private System.Windows.Forms.ComboBox cmbxcolor;
        private System.Windows.Forms.TextBox txtsearch;
        private System.Windows.Forms.GroupBox gbUpdateCar;
        private System.Windows.Forms.ComboBox cmbxcolorgb2;
        private System.Windows.Forms.ComboBox cmbxtransmissiongb2;
        private System.Windows.Forms.ComboBox cmbxfuelgb2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtpricegb2;
        private System.Windows.Forms.TextBox txtyeargb2;
        private System.Windows.Forms.TextBox txtmodelgb2;
        private System.Windows.Forms.TextBox txtbrandgb2;
        private System.Windows.Forms.TextBox txtnumberplategb2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
    }
}